<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'blushmoda3');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '9c95YY');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');
define('fs_method', 'direct');


/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'G3*aq0L/8UWlHI:u?,%@/~dIMs9H%BCbA@_+N]S%Ir8W/@~LfzOohR9aMf(p`Aw-');
define('SECURE_AUTH_KEY',  '%IwIf-{w^`C. e(<m{2FTD1N g})H1;;O[2L<R4t%mbxqh/Va]Bk4!r{-7:B3kP)');
define('LOGGED_IN_KEY',    'G])Bi@f@]P4%V(GmBIK.s&zZx:i1d,`1><<6q%RE08g:CBxQEa_ }<w;DctlIBr&');
define('NONCE_KEY',        'x6tb7|<g.l*nv]t&qn9mi-L$7fyS+(CQ%@Gh)UqNW6.+/r3&*J:_?zF]uZP=kCq!');
define('AUTH_SALT',        'XAqXxtvrSA]E%7+,fh-}H~*s#`8Pj%ove)=0++/M)?!R$P[c-M|{Rc,GcB=GVQYo');
define('SECURE_AUTH_SALT', '%R0G|Lnf_CTvbWy4%}Ba!,N}%L(RJ;(pyE90!B|@i~A0u, LD8Y=TspO5>n,IlD8');
define('LOGGED_IN_SALT',   'ZH8uE}FP5$d)xj^m<`f-Ep}IPOpdHPdJW`@_&L/z%E(-PaBO]-VH<Wm*~K<T^,#h');
define('NONCE_SALT',       '-O8NTHQ<yM}?j9x[xne:ZiEacn0bU}jAJIyPwdAK1XQ6Rf%Za[~ne_1r9XfcCoH_');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
