<?php
/**
 * Plugin support: Mail Chimp
 *
 * @package WordPress
 * @subpackage ThemeREX Addons
 * @since v1.5
 */

// Check if plugin installed and activated
if ( !function_exists( 'trx_addons_exists_easy_booking' ) ) {
	function trx_addons_exists_easy_booking() {
		return class_exists('Easy_booking');
	}
}



// One-click import support
//------------------------------------------------------------------------

// Check plugin in the required plugins
if ( !function_exists( 'trx_addons_easy_booking_importer_required_plugins' ) ) {
	if (is_admin()) add_filter( 'trx_addons_filter_importer_required_plugins',	'trx_addons_easy_booking_importer_required_plugins', 10, 2 );
	function trx_addons_easy_booking_importer_required_plugins($not_installed='', $list='') {
		if (strpos($list, 'woocommerce-easy-booking-system')!==false && !trx_addons_exists_easy_booking() )
			$not_installed .= '<br>' . esc_html__('Woocommerce Easy Booking', 'trx_addons');
		return $not_installed;
	}
}
// Set plugin's specific importer options
if ( !function_exists( 'trx_addons_easy_booking_importer_set_options' ) ) {
	if (is_admin()) add_filter( 'trx_addons_filter_importer_options',	'trx_addons_easy_booking_importer_set_options' );
	function trx_addons_easy_booking_importer_set_options($options=array()) {

		if ( trx_addons_exists_easy_booking() && in_array('woocommerce-easy-booking-system', $options['required_plugins']) ) {
			if (is_array($options)) {
				$options['additional_options'][] = 'easy_booking_%';		// Add slugs to export options for this plugin
			}
		}
		return $options;
	}
}

// Add checkbox to the one-click importer
if ( !function_exists( 'trx_addons_easy_booking_importer_show_params' ) ) {
	if (is_admin()) add_action( 'trx_addons_action_importer_params',	'trx_addons_easy_booking_importer_show_params', 10, 1 );
	function trx_addons_easy_booking_importer_show_params($importer) {
		if ( trx_addons_exists_easy_booking() && in_array('woocommerce-easy-booking-system', $importer->options['required_plugins']) ) {
			$importer->show_importer_params(array(
				'slug' => 'woocommerce-easy-booking-system',
				'title' => esc_html__('Import Woocommerce Easy Booking', 'trx_addons'),
				'part' => 1
			));
		}
	}
}
?>