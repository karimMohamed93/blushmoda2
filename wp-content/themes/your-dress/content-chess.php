<?php
/**
 * The Classic template to display the content
 *
 * Used for index/archive/search.
 *
 * @package WordPress
 * @subpackage YOUR_DRESS
 * @since YOUR_DRESS 1.0
 */

$your_dress_blog_style = explode('_', your_dress_get_theme_option('blog_style'));
$your_dress_columns = empty($your_dress_blog_style[1]) ? 1 : max(1, $your_dress_blog_style[1]);
$your_dress_expanded = !your_dress_sidebar_present() && your_dress_is_on(your_dress_get_theme_option('expand_content'));
$your_dress_post_format = get_post_format();
$your_dress_post_format = empty($your_dress_post_format) ? 'standard' : str_replace('post-format-', '', $your_dress_post_format);
$your_dress_animation = your_dress_get_theme_option('blog_animation');

?><article id="post-<?php the_ID(); ?>" 
	<?php post_class( 'post_item post_layout_chess post_layout_chess_'.esc_attr($your_dress_columns).' post_format_'.esc_attr($your_dress_post_format) ); ?>
	<?php echo (!your_dress_is_off($your_dress_animation) ? ' data-animation="'.esc_attr(your_dress_get_animation_classes($your_dress_animation)).'"' : ''); ?>
	>

	<?php
	// Add anchor
	if ($your_dress_columns == 1 && shortcode_exists('trx_sc_anchor')) {
		echo do_shortcode('[trx_sc_anchor id="post_'.esc_attr(get_the_ID()).'" title="'.esc_attr(get_the_title()).'"]');
	}

	// Featured image
	your_dress_show_post_featured( array(
											'class' => $your_dress_columns == 1 ? 'trx-stretch-height' : '',
											'show_no_image' => true,
											'thumb_bg' => true,
											'thumb_size' => your_dress_get_thumb_size(
																	strpos(your_dress_get_theme_option('body_style'), 'full')!==false
																		? ( $your_dress_columns > 1 ? 'huge' : 'original' )
																		: (	$your_dress_columns > 2 ? 'big' : 'huge')
																	)
											) 
										);

	?><div class="post_inner"><div class="post_inner_content"><?php 

		?><div class="post_header entry-header"><?php 
			do_action('your_dress_action_before_post_title'); 

			// Post title
			the_title( sprintf( '<h3 class="post_title entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h3>' );
			
			do_action('your_dress_action_before_post_meta'); 

			// Post meta
			$your_dress_post_meta = your_dress_show_post_meta(array(
									'categories' => $your_dress_columns < 2 ? true : false,
									'date' => false,
									'edit' => $your_dress_columns == 1,
									'seo' => false,
									'share' => false,
									'counters' => $your_dress_columns < 2 ? 'comments' : '',
									'echo' => false
									)
								);
			your_dress_show_layout($your_dress_post_meta);
		?></div><!-- .entry-header -->
	
		<div class="post_content entry-content">
			<div class="post_content_inner">
				<?php
				$your_dress_show_learn_more = !in_array($your_dress_post_format, array('link', 'aside', 'status', 'quote'));
				if (has_excerpt()) {
					the_excerpt();
				} else if (strpos(get_the_content('!--more'), '!--more')!==false) {
					the_content( '' );
				} else if (in_array($your_dress_post_format, array('link', 'aside', 'status', 'quote'))) {
					the_content();
				} else if (substr(get_the_content(), 0, 1)!='[') {
					the_excerpt();
				}
				?>
			</div>
			<?php
			// Post meta
			if (in_array($your_dress_post_format, array('link', 'aside', 'status', 'quote'))) {
				your_dress_show_layout($your_dress_post_meta);
			}
			// More button
			if ( $your_dress_show_learn_more ) {
				?><p><a class="more-link" href="<?php echo esc_url(get_permalink()); ?>"><?php esc_html_e('Read more', 'your-dress'); ?></a></p><?php
			}
			?>
		</div><!-- .entry-content -->

	</div></div><!-- .post_inner -->

</article>