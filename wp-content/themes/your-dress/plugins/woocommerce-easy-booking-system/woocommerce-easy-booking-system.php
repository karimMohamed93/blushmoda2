<?php
/* WooCommerce Easy Booking support functions
------------------------------------------------------------------------------- */

// Theme init priorities:
// 9 - register other filters (for installer, etc.)
if (!function_exists('your_dress_easy_booking_theme_setup9')) {
	add_action('after_setup_theme', 'your_dress_easy_booking_theme_setup9', 9);
	function your_dress_easy_booking_theme_setup9() {

		if (is_admin()) {
			add_filter( 'your_dress_filter_tgmpa_required_plugins',		'your_dress_easy_booking_tgmpa_required_plugins' );
		}

		if (!your_dress_exists_easy_booking()) { return ''; }

		add_action('wp_enqueue_scripts',				'your_dress_easy_booking_frontend_scripts', 1100);
		add_filter( 'your_dress_filter_merge_styles',	'your_dress_easy_booking_merge_styles' );

		//Change plugin WooCommerce Easy Booking subtitle
		add_filter('easy_booking_loop_add_to_cart_link', 'your_dress_easy_booking_loop_add_to_cart_link', 10, 2);

		// Change text on 'Add to cart' button if product is bookable
		add_filter(    'woocommerce_product_single_add_to_cart_text','your_dress_easy_booking_add_to_cart_text' );

	}
}


if (!function_exists('your_dress_exists_easy_booking')) {
	function your_dress_exists_easy_booking() {
		return class_exists('Easy_booking');
	}
}

// Filter to add in the required plugins list
if ( !function_exists( 'your_dress_easy_booking_tgmpa_required_plugins' ) ) {
	//Handler of the add_filter('your_dress_filter_tgmpa_required_plugins',	'your_dress_easy_booking_tgmpa_required_plugins');
	function your_dress_easy_booking_tgmpa_required_plugins($list=array()) {
		if (in_array('woocommerce-easy-booking-system', (array)your_dress_storage_get('required_plugins'))) {
			// woocommerce-easy-booking-system plugin
			$list[] = array(
				'name' 		=> esc_html__('Woocommerce Easy Booking', 'your-dress'),
				'slug' 		=> 'woocommerce-easy-booking-system',
				'required' 	=> false
			);

		}
		return $list;
	}
}



if (!function_exists('your_dress_easy_booking_add_to_cart_text')) {
	//add_filter(    'woocommerce_product_single_add_to_cart_text','your_dress_easy_booking_add_to_cart_text' );
	function your_dress_easy_booking_add_to_cart_text($text) {
		global $product;

		if (function_exists('wceb_is_bookable') && wceb_is_bookable($product)) {
			return esc_html__('Rent a dress', 'your-dress');
		}

		return $text;
	}
}

if (!function_exists('your_dress_easy_booking_loop_add_to_cart_link')) {
	//add_filter( 'easy_booking_loop_add_to_cart_link', 'your_dress_easy_booking_loop_add_to_cart_link', 10, 2 );
	function your_dress_easy_booking_loop_add_to_cart_link( $content, $product ) {
		$product_id = is_callable( array( $product, 'get_id' ) ) ? $product->get_id() : $product->id;
		$link    = get_permalink( $product_id );
		$label   = __( 'Rent a dress', 'your-dress' );
		$content = '<a href="' . esc_url( $link ) . '" rel="nofollow" class="button your-dress-rent-button icon-rent"><span>' . esc_html( $label  ) . '</span></a>';

		return $content;
	}
}


// Enqueue WooCommerce custom styles
if ( !function_exists( 'your_dress_easy_booking_frontend_scripts' ) ) {
	//Handler of the add_action( 'wp_enqueue_scripts', 'your_dress_easy_booking_frontend_scripts', 1100 );
	function your_dress_easy_booking_frontend_scripts() {
		//if (your_dress_is_woocommerce_page())
		if (your_dress_is_on(your_dress_get_theme_option('debug_mode')) && your_dress_get_file_dir('plugins/woocommerce-easy-booking-system/woocommerce-easy-booking-system.css')!='')
			wp_enqueue_style( 'your_dress-woocommerce-easy-booking-system',  your_dress_get_file_url('plugins/woocommerce-easy-booking-system/woocommerce-easy-booking-system.css'), array(), null );
	}
}


// Merge custom styles
if ( !function_exists( 'your_dress_easy_booking_merge_styles' ) ) {
	//Handler of the add_filter('your_dress_filter_merge_styles', 'your_dress_easy_booking_merge_styles');
	function your_dress_easy_booking_merge_styles($list) {
		$list[] = 'plugins/woocommerce-easy-booking-system/woocommerce-easy-booking-system.css';
		return $list;
	}
}
