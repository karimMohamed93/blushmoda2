<?php
// Add plugin-specific colors and fonts to the custom CSS
if (!function_exists('your_dress_mailchimp_get_css')) {
	add_filter('your_dress_filter_get_css', 'your_dress_mailchimp_get_css', 10, 4);
	function your_dress_mailchimp_get_css($css, $colors, $fonts, $scheme='') {
		
		if (isset($css['fonts']) && $fonts) {
			$css['fonts'] .= <<<CSS

CSS;
		
			
			$rad = your_dress_get_border_radius();
			$css['fonts'] .= <<<CSS

.mc4wp-form .mc4wp-form-fields input[type="email"],
.mc4wp-form .mc4wp-form-fields input[type="submit"] {
	-webkit-border-radius: {$rad};
	    -ms-border-radius: {$rad};
			border-radius: {$rad};
}

CSS;
		}

		
		if (isset($css['colors']) && $colors) {
			$css['colors'] .= <<<CSS

.mc4wp-form input[type="email"] {

}

CSS;
		}

		return $css;
	}
}
?>