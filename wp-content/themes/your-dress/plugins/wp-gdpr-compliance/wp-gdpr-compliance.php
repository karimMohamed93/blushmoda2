<?php
/* gdpr-compliance support functions
------------------------------------------------------------------------------- */

// Theme init priorities:
// 9 - register other filters (for installer, etc.)
if (!function_exists('your_dress_gdpr_theme_setup9')) {
	add_action( 'after_setup_theme', 'your_dress_gdpr_theme_setup9', 9 );
	function your_dress_gdpr_theme_setup9() {

		if (your_dress_exists_gdpr()) {
			add_filter( 'your_dress_filter_merge_styles',						'your_dress_gdpr_merge_styles' );
		}
		if (is_admin()) {
			add_filter( 'your_dress_filter_tgmpa_required_plugins',			'your_dress_gdpr_tgmpa_required_plugins' );
		}
	}
}

// Filter to add in the required plugins list
if ( !function_exists( 'your_dress_gdpr_tgmpa_required_plugins' ) ) {
	//Handler of the add_filter('your_dress_filter_tgmpa_required_plugins',	'your_dress_gdpr_tgmpa_required_plugins');
	function your_dress_gdpr_tgmpa_required_plugins($list=array()) {
		if (in_array('wp-gdpr-compliance', (array)your_dress_storage_get('required_plugins'))) {
			// CF7 plugin
			$list[] = array(
				'name' 		=> esc_html__('WP GDPR Compliance', 'your-dress'),
				'slug' 		=> 'wp-gdpr-compliance',
				'required' 	=> false
			);

		}
		return $list;
	}
}


// Check if gdpr installed and activated
if ( !function_exists( 'your_dress_exists_gdpr' ) ) {
	function your_dress_exists_gdpr() {
		return class_exists('GDPR_VERSION');
	}
}