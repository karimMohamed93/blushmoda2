<?php
/* Essential Grid support functions
------------------------------------------------------------------------------- */


// Theme init priorities:
// 9 - register other filters (for installer, etc.)
if (!function_exists('your_dress_essential_grid_theme_setup9')) {
	add_action( 'after_setup_theme', 'your_dress_essential_grid_theme_setup9', 9 );
	function your_dress_essential_grid_theme_setup9() {
		if (your_dress_exists_essential_grid()) {
			add_action( 'wp_enqueue_scripts', 							'your_dress_essential_grid_frontend_scripts', 1100 );
			add_filter( 'your_dress_filter_merge_styles',					'your_dress_essential_grid_merge_styles' );
		}
		if (is_admin()) {
			add_filter( 'your_dress_filter_tgmpa_required_plugins',		'your_dress_essential_grid_tgmpa_required_plugins' );
		}
	}
}

// Check if plugin installed and activated
if ( !function_exists( 'your_dress_exists_essential_grid' ) ) {
	function your_dress_exists_essential_grid() {
		return defined('EG_PLUGIN_PATH');
	}
}

// Filter to add in the required plugins list
if ( !function_exists( 'your_dress_essential_grid_tgmpa_required_plugins' ) ) {
	//Handler of the add_filter('your_dress_filter_tgmpa_required_plugins',	'your_dress_essential_grid_tgmpa_required_plugins');
	function your_dress_essential_grid_tgmpa_required_plugins($list=array()) {
		if (in_array('essential-grid', (array)your_dress_storage_get('required_plugins'))) {
			$path = your_dress_get_file_dir('plugins/essential-grid/essential-grid.zip');
			$list[] = array(
						'name' 		=> esc_html__('Essential Grid', 'your-dress'),
						'slug' 		=> 'essential-grid',
						'source'	=> !empty($path) ? $path : 'upload://essential-grid.zip',
						'required' 	=> false
			);
		}
		return $list;
	}
}
	
// Enqueue plugin's custom styles
if ( !function_exists( 'your_dress_essential_grid_frontend_scripts' ) ) {
	//Handler of the add_action( 'wp_enqueue_scripts', 'your_dress_essential_grid_frontend_scripts', 1100 );
	function your_dress_essential_grid_frontend_scripts() {
		if (your_dress_is_on(your_dress_get_theme_option('debug_mode')) && your_dress_get_file_dir('plugins/essential-grid/essential-grid.css')!='')
			wp_enqueue_style( 'your_dress-essential-grid',  your_dress_get_file_url('plugins/essential-grid/essential-grid.css'), array(), null );
	}
}
	
// Merge custom styles
if ( !function_exists( 'your_dress_essential_grid_merge_styles' ) ) {
	//Handler of the add_filter('your_dress_filter_merge_styles', 'your_dress_essential_grid_merge_styles');
	function your_dress_essential_grid_merge_styles($list) {
		$list[] = 'plugins/essential-grid/essential-grid.css';
		return $list;
	}
}
?>