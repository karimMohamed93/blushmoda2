<?php
/* Contact Form 7 support functions
------------------------------------------------------------------------------- */

// Theme init priorities:
// 9 - register other filters (for installer, etc.)
if (!function_exists('your_dress_cf7_theme_setup9')) {
	add_action( 'after_setup_theme', 'your_dress_cf7_theme_setup9', 9 );
	function your_dress_cf7_theme_setup9() {
		
		if (your_dress_exists_cf7()) {
			add_action( 'wp_enqueue_scripts', 								'your_dress_cf7_frontend_scripts', 1100 );
			add_filter( 'your_dress_filter_merge_styles',						'your_dress_cf7_merge_styles' );
		}
		if (is_admin()) {
			add_filter( 'your_dress_filter_tgmpa_required_plugins',			'your_dress_cf7_tgmpa_required_plugins' );
		}
	}
}

// Filter to add in the required plugins list
if ( !function_exists( 'your_dress_cf7_tgmpa_required_plugins' ) ) {
	//Handler of the add_filter('your_dress_filter_tgmpa_required_plugins',	'your_dress_cf7_tgmpa_required_plugins');
	function your_dress_cf7_tgmpa_required_plugins($list=array()) {
		if (in_array('contact-form-7', (array)your_dress_storage_get('required_plugins'))) {
			// CF7 plugin
			$list[] = array(
					'name' 		=> esc_html__('Contact Form 7', 'your-dress'),
					'slug' 		=> 'contact-form-7',
					'required' 	=> false
			);

		}
		return $list;
	}
}



// Check if cf7 installed and activated
if ( !function_exists( 'your_dress_exists_cf7' ) ) {
	function your_dress_exists_cf7() {
		return class_exists('WPCF7');
	}
}
	
// Enqueue custom styles
if ( !function_exists( 'your_dress_cf7_frontend_scripts' ) ) {
	//Handler of the add_action( 'wp_enqueue_scripts', 'your_dress_cf7_frontend_scripts', 1100 );
	function your_dress_cf7_frontend_scripts() {
		if (your_dress_is_on(your_dress_get_theme_option('debug_mode')) && your_dress_get_file_dir('plugins/contact-form-7/contact-form-7.css')!='')
			wp_enqueue_style( 'your_dress-contact-form-7',  your_dress_get_file_url('plugins/contact-form-7/contact-form-7.css'), array(), null );
	}
}
	
// Merge custom styles
if ( !function_exists( 'your_dress_cf7_merge_styles' ) ) {
	//Handler of the add_filter('your_dress_filter_merge_styles', 'your_dress_cf7_merge_styles');
	function your_dress_cf7_merge_styles($list) {
		$list[] = 'plugins/contact-form-7/contact-form-7.css';
		return $list;
	}
}
?>