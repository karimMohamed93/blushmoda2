<?php
/* Revolution Slider support functions
------------------------------------------------------------------------------- */

// Theme init priorities:
// 9 - register other filters (for installer, etc.)
if (!function_exists('your_dress_revslider_theme_setup9')) {
	add_action( 'after_setup_theme', 'your_dress_revslider_theme_setup9', 9 );
	function your_dress_revslider_theme_setup9() {
		if (is_admin()) {
			add_filter( 'your_dress_filter_tgmpa_required_plugins',	'your_dress_revslider_tgmpa_required_plugins' );
		}
	}
}

// Check if RevSlider installed and activated
if ( !function_exists( 'your_dress_exists_revslider' ) ) {
	function your_dress_exists_revslider() {
		return function_exists('rev_slider_shortcode');
	}
}

// Filter to add in the required plugins list
if ( !function_exists( 'your_dress_revslider_tgmpa_required_plugins' ) ) {
	//Handler of the add_filter('your_dress_filter_tgmpa_required_plugins',	'your_dress_revslider_tgmpa_required_plugins');
	function your_dress_revslider_tgmpa_required_plugins($list=array()) {
		if (in_array('revslider', (array)your_dress_storage_get('required_plugins'))) {
			$path = your_dress_get_file_dir('plugins/revslider/revslider.zip');
			$list[] = array(
					'name' 		=> esc_html__('Revolution Slider', 'your-dress'),
					'slug' 		=> 'revslider',
					'source'	=> !empty($path) ? $path : 'upload://revslider.zip',
					'required' 	=> false
			);
		}
		return $list;
	}
}
?>