<?php
/* Woocommerce support functions
------------------------------------------------------------------------------- */

// Theme init priorities:
// 1 - register filters, that add/remove lists items for the Theme Options
if (!function_exists('your_dress_woocommerce_theme_setup1')) {
	add_action( 'after_setup_theme', 'your_dress_woocommerce_theme_setup1', 1 );
	function your_dress_woocommerce_theme_setup1() {

		add_theme_support( 'woocommerce', array('gallery_thumbnail_image_width' => 136) );

        // Next setting from the WooCommerce 3.0+ enable built-in image zoom on the single product page
        add_theme_support( 'wc-product-gallery-zoom' );

        // Next setting from the WooCommerce 3.0+ enable built-in image slider on the single product page
        add_theme_support( 'wc-product-gallery-slider' );

        // Next setting from the WooCommerce 3.0+ enable built-in image lightbox on the single product page
        add_theme_support( 'wc-product-gallery-lightbox' );
		
		add_filter( 'your_dress_filter_list_sidebars', 	'your_dress_woocommerce_list_sidebars' );
		add_filter( 'your_dress_filter_list_posts_types',	'your_dress_woocommerce_list_post_types');
		add_filter( 'woocommerce_get_image_size_gallery_thumbnail',	'your_dress_woocommerce_get_image_size_gallery_thumbnail');


		//add_filter( 'product_cat_class', 'your_dress_filter_product_cat_class', 10, 3 );
	}
}

// Theme init priorities:
// 3 - add/remove Theme Options elements
if (!function_exists('your_dress_woocommerce_theme_setup3')) {
	add_action( 'after_setup_theme', 'your_dress_woocommerce_theme_setup3', 3 );
	function your_dress_woocommerce_theme_setup3() {
		if (your_dress_exists_woocommerce()) {
		
			your_dress_storage_merge_array('options', '', array(
				// Section 'WooCommerce' - settings for show pages
				'shop' => array(
					"title" => esc_html__('Shop', 'your-dress'),
					"desc" => wp_kses_data( __('Select parameters to display the shop pages', 'your-dress') ),
					"type" => "section"
					),
				'expand_content_shop' => array(
					"title" => esc_html__('Expand content', 'your-dress'),
					"desc" => wp_kses_data( __('Expand the content width if the sidebar is hidden', 'your-dress') ),
					"refresh" => false,
					"std" => 1,
					"type" => "checkbox"
					),
				'blog_columns_shop' => array(
					"title" => esc_html__('Shop loop columns', 'your-dress'),
					"desc" => wp_kses_data( __('How many columns should be used in the shop loop (from 2 to 4)?', 'your-dress') ),
					"std" => 2,
					"options" => your_dress_get_list_range(2,4),
					"type" => "select"
					),
				'related_posts_shop' => array(
					"title" => esc_html__('Related products', 'your-dress'),
					"desc" => wp_kses_data( __('How many related products should be displayed in the single product page  (from 2 to 4)?', 'your-dress') ),
					"std" => 2,
					"options" => your_dress_get_list_range(2,4),
					"type" => "select"
					),
				'shop_mode' => array(
					"title" => esc_html__('Shop mode', 'your-dress'),
					"desc" => wp_kses_data( __('Select style for the products list', 'your-dress') ),
					"std" => 'thumbs',
					"options" => array(
						'thumbs'=> esc_html__('Thumbnails', 'your-dress'),
						'list'	=> esc_html__('List', 'your-dress'),
					),
					"type" => "select"
					),
				'shop_hover' => array(
					"title" => esc_html__('Hover style', 'your-dress'),
					"desc" => wp_kses_data( __('Hover style on the products in the shop archive', 'your-dress') ),
					"std" => 'shop',
					"options" => apply_filters('your_dress_filter_shop_hover', array(
						'none' => esc_html__('None', 'your-dress'),
						'shop' => esc_html__('Icons', 'your-dress'),
						'shop_buttons' => esc_html__('Buttons', 'your-dress')
					)),
					"type" => "select"
					),
				'header_style_shop' => array(
					"title" => esc_html__('Header style', 'your-dress'),
					"desc" => wp_kses_data( __('Select style to display the site header on the shop archive', 'your-dress') ),
					"std" => 'inherit',
					"options" => array(),
					"type" => "select"
					),
				'header_position_shop' => array(
					"title" => esc_html__('Header position', 'your-dress'),
					"desc" => wp_kses_data( __('Select position to display the site header on the shop archive', 'your-dress') ),
					"std" => 'inherit',
					"options" => array(),
					"type" => "select"
					),
				'header_widgets_shop' => array(
					"title" => esc_html__('Header widgets', 'your-dress'),
					"desc" => wp_kses_data( __('Select set of widgets to show in the header on the shop pages', 'your-dress') ),
					"std" => 'hide',
					"options" => array(),
					"type" => "select"
					),
				'sidebar_widgets_shop' => array(
					"title" => esc_html__('Sidebar widgets', 'your-dress'),
					"desc" => wp_kses_data( __('Select sidebar to show on the shop pages', 'your-dress') ),
					"std" => 'woocommerce_widgets',
					"options" => array(),
					"type" => "select"
					),
				'sidebar_position_shop' => array(
					"title" => esc_html__('Sidebar position', 'your-dress'),
					"desc" => wp_kses_data( __('Select position to show sidebar on the shop pages', 'your-dress') ),
					"refresh" => false,
					"std" => 'left',
					"options" => array(),
					"type" => "select"
					),
				'hide_sidebar_on_single_shop' => array(
					"title" => esc_html__('Hide sidebar on the single product', 'your-dress'),
					"desc" => wp_kses_data( __("Hide sidebar on the single product's page", 'your-dress') ),
					"std" => 0,
					"type" => "checkbox"
					),
				'widgets_above_page_shop' => array(
					"title" => esc_html__('Widgets above the page', 'your-dress'),
					"desc" => wp_kses_data( __('Select widgets to show above page (content and sidebar)', 'your-dress') ),
					"std" => 'hide',
					"options" => array(),
					"type" => "select"
					),
				'widgets_above_content_shop' => array(
					"title" => esc_html__('Widgets above the content', 'your-dress'),
					"desc" => wp_kses_data( __('Select widgets to show at the beginning of the content area', 'your-dress') ),
					"std" => 'hide',
					"options" => array(),
					"type" => "select"
					),
				'widgets_below_content_shop' => array(
					"title" => esc_html__('Widgets below the content', 'your-dress'),
					"desc" => wp_kses_data( __('Select widgets to show at the ending of the content area', 'your-dress') ),
					"std" => 'hide',
					"options" => array(),
					"type" => "select"
					),
				'widgets_below_page_shop' => array(
					"title" => esc_html__('Widgets below the page', 'your-dress'),
					"desc" => wp_kses_data( __('Select widgets to show below the page (content and sidebar)', 'your-dress') ),
					"std" => 'hide',
					"options" => array(),
					"type" => "select"
					),
				'footer_scheme_shop' => array(
					"title" => esc_html__('Footer Color Scheme', 'your-dress'),
					"desc" => wp_kses_data( __('Select color scheme to decorate footer area', 'your-dress') ),
					"std" => 'dark',
					"options" => array(),
					"type" => "select"
					),
				'footer_widgets_shop' => array(
					"title" => esc_html__('Footer widgets', 'your-dress'),
					"desc" => wp_kses_data( __('Select set of widgets to show in the footer', 'your-dress') ),
					"std" => 'footer_widgets',
					"options" => array(),
					"type" => "select"
					),
				'footer_columns_shop' => array(
					"title" => esc_html__('Footer columns', 'your-dress'),
					"desc" => wp_kses_data( __('Select number columns to show widgets in the footer. If 0 - autodetect by the widgets count', 'your-dress') ),
					"dependency" => array(
						'footer_widgets_shop' => array('^hide')
					),
					"std" => 0,
					"options" => your_dress_get_list_range(0,6),
					"type" => "select"
					),
				'footer_wide_shop' => array(
					"title" => esc_html__('Footer fullwide', 'your-dress'),
					"desc" => wp_kses_data( __('Do you want to stretch the footer to the entire window width?', 'your-dress') ),
					"std" => 0,
					"type" => "checkbox"
					)
				)
			);
		}
	}
}

// Theme init priorities:
// 9 - register other filters (for installer, etc.)
if (!function_exists('your_dress_woocommerce_theme_setup9')) {
	add_action( 'after_setup_theme', 'your_dress_woocommerce_theme_setup9', 9 );
	function your_dress_woocommerce_theme_setup9() {
		
		if (your_dress_exists_woocommerce()) {
			add_action( 'wp_enqueue_scripts', 								'your_dress_woocommerce_frontend_scripts', 1100 );
			add_filter( 'your_dress_filter_merge_styles',						'your_dress_woocommerce_merge_styles' );
			add_filter( 'your_dress_filter_get_post_info',		 				'your_dress_woocommerce_get_post_info');
			add_filter( 'your_dress_filter_post_type_taxonomy',				'your_dress_woocommerce_post_type_taxonomy', 10, 2 );
			if (!is_admin()) {
				add_filter( 'your_dress_filter_detect_blog_mode',				'your_dress_woocommerce_detect_blog_mode' );
				add_filter( 'your_dress_filter_get_post_categories', 			'your_dress_woocommerce_get_post_categories');
				add_filter( 'your_dress_filter_allow_override_header_image',	'your_dress_woocommerce_allow_override_header_image' );
				add_action( 'your_dress_action_before_post_meta',				'your_dress_woocommerce_action_before_post_meta');
			}
		}
		if (is_admin()) {
			add_filter( 'your_dress_filter_tgmpa_required_plugins',			'your_dress_woocommerce_tgmpa_required_plugins' );
		}

		// Add wrappers and classes to the standard WooCommerce output
		if (your_dress_exists_woocommerce()) {

			// Remove WOOC sidebar
			remove_action( 'woocommerce_sidebar', 						'woocommerce_get_sidebar', 10 );

			// Remove link around product item
			remove_action('woocommerce_before_shop_loop_item',			'woocommerce_template_loop_product_link_open', 10);
			remove_action('woocommerce_after_shop_loop_item',			'woocommerce_template_loop_product_link_close', 5);

			// Remove add_to_cart button
			//remove_action('woocommerce_after_shop_loop_item',			'woocommerce_template_loop_add_to_cart', 10);
			
			// Remove link around product category
			remove_action('woocommerce_before_subcategory',				'woocommerce_template_loop_category_link_open', 10);
			remove_action('woocommerce_after_subcategory',				'woocommerce_template_loop_category_link_close', 10);

			/**
			 * Subcategories.
			 *
			 * @see woocommerce_subcategory_thumbnail()
			 */
			remove_action( 'woocommerce_before_subcategory_title', 'woocommerce_subcategory_thumbnail', 10 );
			add_action( 'woocommerce_before_subcategory_title', 'your_dress_woocommerce_subcategory_thumbnail', 10 );

			// Open main content wrapper - <article>
			remove_action( 'woocommerce_before_main_content',			'woocommerce_output_content_wrapper', 10);
			add_action(    'woocommerce_before_main_content',			'your_dress_woocommerce_wrapper_start', 10);
			// Close main content wrapper - </article>
			remove_action( 'woocommerce_after_main_content',			'woocommerce_output_content_wrapper_end', 10);		
			add_action(    'woocommerce_after_main_content',			'your_dress_woocommerce_wrapper_end', 10);

			// Close header section
			add_action(    'woocommerce_archive_description',			'your_dress_woocommerce_archive_description', 15 );

			// Add theme specific search form
			add_filter(    'get_product_search_form',					'your_dress_woocommerce_get_product_search_form' );

			// Add list mode buttons
			add_action(    'woocommerce_before_shop_loop', 				'your_dress_woocommerce_before_shop_loop', 10 );

			// Set columns number for the products loop
			add_filter(    'loop_shop_columns',							'your_dress_woocommerce_loop_shop_columns' );
			add_filter(    'post_class',								'your_dress_woocommerce_loop_shop_columns_class' );
			add_filter(    'product_cat_class',							'your_dress_woocommerce_loop_shop_columns_class', 10, 3 );
			// Open product/category item wrapper
			add_action(    'woocommerce_before_subcategory_title',		'your_dress_woocommerce_item_wrapper_start', 9 );
			add_action(    'woocommerce_before_shop_loop_item_title',	'your_dress_woocommerce_item_wrapper_start', 9 );
			// Close featured image wrapper and open title wrapper
			add_action(    'woocommerce_before_subcategory_title',		'your_dress_woocommerce_title_wrapper_start', 20 );
			add_action(    'woocommerce_before_shop_loop_item_title',	'your_dress_woocommerce_title_wrapper_start', 20 );

			// Add tags before title
			add_action(    'woocommerce_before_shop_loop_item_title',	'your_dress_woocommerce_title_tags', 30 );

			// Wrap product title into link
			add_action(    'the_title',									'your_dress_woocommerce_the_title');
			// Wrap category title into link
			remove_action( 'woocommerce_shop_loop_subcategory_title', 'woocommerce_template_loop_category_title', 10 );
			add_action(		'woocommerce_shop_loop_subcategory_title',  'your_dress_woocommerce_shop_loop_subcategory_title', 9, 1);

			// Close title wrapper and add description in the list mode
			add_action(    'woocommerce_after_shop_loop_item_title',	'your_dress_woocommerce_title_wrapper_end', 7);
			add_action(    'woocommerce_after_subcategory_title',		'your_dress_woocommerce_title_wrapper_end2', 10 );
			// Close product/category item wrapper
			add_action(    'woocommerce_after_subcategory',				'your_dress_woocommerce_item_wrapper_end', 20 );
			add_action(    'woocommerce_after_shop_loop_item',			'your_dress_woocommerce_item_wrapper_end', 20 );

			// Add product ID into product meta section (after categories and tags)
			add_action(    'woocommerce_product_meta_end',				'your_dress_woocommerce_show_product_id', 10);
			
			// Set columns number for the product's thumbnails
			add_filter(    'woocommerce_product_thumbnails_columns',	'your_dress_woocommerce_product_thumbnails_columns' );

			// Set columns number for the related products
			add_filter(    'woocommerce_output_related_products_args',	'your_dress_woocommerce_output_related_products_args' );

            remove_action( 'woocommerce_after_shop_loop_item_title',    'woocommerce_template_loop_price', 10 );

	
			// Detect current shop mode
			if (!is_admin()) {
				$shop_mode = your_dress_get_value_gpc('your_dress_shop_mode');
				if (empty($shop_mode) && your_dress_check_theme_option('shop_mode'))
					$shop_mode = your_dress_get_theme_option('shop_mode');
				if (empty($shop_mode))
					$shop_mode = 'thumbs';
				your_dress_storage_set('shop_mode', $shop_mode);
			}
		}
	}
}

// Check if WooCommerce installed and activated
if ( !function_exists( 'your_dress_exists_woocommerce' ) ) {
	function your_dress_exists_woocommerce() {
		return class_exists('Woocommerce');
	}
}

// Return true, if current page is any woocommerce page
if ( !function_exists( 'your_dress_woocommerce_get_image_size_gallery_thumbnail' ) ) {
	//add_filter( 'woocommerce_get_image_size_gallery_thumbnail',	'your_dress_woocommerce_get_image_size_gallery_thumbnail', 10, 2);
	function your_dress_woocommerce_get_image_size_gallery_thumbnail($size) {
		$size['height'] = 216;
		return $size;
	}
}

// Return true, if current page is any woocommerce page
if ( !function_exists( 'your_dress_is_woocommerce_page' ) ) {
	function your_dress_is_woocommerce_page() {
		$rez = false;
		if (your_dress_exists_woocommerce())
			$rez = is_woocommerce() || is_shop() || is_product() || is_product_category() || is_product_tag() || is_product_taxonomy() || is_cart() || is_checkout() || is_account_page();
		return $rez;
	}
}

// Detect current blog mode
if ( !function_exists( 'your_dress_woocommerce_detect_blog_mode' ) ) {
	//Handler of the add_filter( 'your_dress_filter_detect_blog_mode', 'your_dress_woocommerce_detect_blog_mode' );
	function your_dress_woocommerce_detect_blog_mode($mode='') {
		if (is_shop() || is_product_category() || is_product_tag() || is_product_taxonomy())
			$mode = 'shop';
		else if (is_product() || is_cart() || is_checkout() || is_account_page())
			$mode = 'shop';	//'shop_single';
		return $mode;
	}
}

// Return taxonomy for current post type
if ( !function_exists( 'your_dress_woocommerce_post_type_taxonomy' ) ) {
	//Handler of the add_filter( 'your_dress_filter_post_type_taxonomy',	'your_dress_woocommerce_post_type_taxonomy', 10, 2 );
	function your_dress_woocommerce_post_type_taxonomy($tax='', $post_type='') {
		if ($post_type == 'product')
			$tax = 'product_cat';
		return $tax;
	}
}

// Return true if page title section is allowed
if ( !function_exists( 'your_dress_woocommerce_allow_override_header_image' ) ) {
	//Handler of the add_filter( 'your_dress_filter_allow_override_header_image', 'your_dress_woocommerce_allow_override_header_image' );
	function your_dress_woocommerce_allow_override_header_image($allow=true) {
		return is_product() ? false : $allow;
	}
}

// Return shop page ID
if ( !function_exists( 'your_dress_woocommerce_get_shop_page_id' ) ) {
	function your_dress_woocommerce_get_shop_page_id() {
		return get_option('woocommerce_shop_page_id');
	}
}

// Return shop page link
if ( !function_exists( 'your_dress_woocommerce_get_shop_page_link' ) ) {
	function your_dress_woocommerce_get_shop_page_link() {
		$url = '';
		$id = your_dress_woocommerce_get_shop_page_id();
		if ($id) $url = get_permalink($id);
		return $url;
	}
}

// Show categories of the current product
if ( !function_exists( 'your_dress_woocommerce_get_post_categories' ) ) {
	//Handler of the add_filter( 'your_dress_filter_get_post_categories', 		'your_dress_woocommerce_get_post_categories');
	function your_dress_woocommerce_get_post_categories($cats='') {
		if (get_post_type()=='product') {
			$cats = your_dress_get_post_terms(', ', get_the_ID(), 'product_cat');
		}
		return $cats;
	}
}

// Add 'product' to the list of the supported post-types
if ( !function_exists( 'your_dress_woocommerce_list_post_types' ) ) {
	//Handler of the add_filter( 'your_dress_filter_list_posts_types', 'your_dress_woocommerce_list_post_types');
	function your_dress_woocommerce_list_post_types($list=array()) {
		$list['product'] = esc_html__('Products', 'your-dress');
		return $list;
	}
}

// Show price of the current product in the widgets and search results
if ( !function_exists( 'your_dress_woocommerce_get_post_info' ) ) {
	//Handler of the add_filter( 'your_dress_filter_get_post_info', 'your_dress_woocommerce_get_post_info');
	function your_dress_woocommerce_get_post_info($post_info='') {
		if (get_post_type()=='product') {
			global $product;
			if ( $price_html = $product->get_price_html() ) {
				$post_info = '<div class="post_price product_price price">' . trim($price_html) . '</div>' . $post_info;
			}
		}
		return $post_info;
	}
}

// Show price of the current product in the search results streampage
if ( !function_exists( 'your_dress_woocommerce_action_before_post_meta' ) ) {
	//Handler of the add_action( 'your_dress_action_before_post_meta', 'your_dress_woocommerce_action_before_post_meta');
	function your_dress_woocommerce_action_before_post_meta() {
		if (get_post_type()=='product') {
			global $product;
			if ( $price_html = $product->get_price_html() ) {
				?><div class="post_price product_price price"><?php your_dress_show_layout($price_html); ?></div><?php
			}
		}
	}
}
	
// Enqueue WooCommerce custom styles
if ( !function_exists( 'your_dress_woocommerce_frontend_scripts' ) ) {
	//Handler of the add_action( 'wp_enqueue_scripts', 'your_dress_woocommerce_frontend_scripts', 1100 );
	function your_dress_woocommerce_frontend_scripts() {
		//if (your_dress_is_woocommerce_page())
			if (your_dress_is_on(your_dress_get_theme_option('debug_mode')) && your_dress_get_file_dir('plugins/woocommerce/woocommerce.css')!='')
				wp_enqueue_style( 'your_dress-woocommerce',  your_dress_get_file_url('plugins/woocommerce/woocommerce.css'), array(), null );
	}
}
	
// Merge custom styles
if ( !function_exists( 'your_dress_woocommerce_merge_styles' ) ) {
	//Handler of the add_filter('your_dress_filter_merge_styles', 'your_dress_woocommerce_merge_styles');
	function your_dress_woocommerce_merge_styles($list) {
		$list[] = 'plugins/woocommerce/woocommerce.css';
		return $list;
	}
}

// Filter to add in the required plugins list
if ( !function_exists( 'your_dress_woocommerce_tgmpa_required_plugins' ) ) {
	//Handler of the add_filter('your_dress_filter_tgmpa_required_plugins',	'your_dress_woocommerce_tgmpa_required_plugins');
	function your_dress_woocommerce_tgmpa_required_plugins($list=array()) {
		if (in_array('woocommerce', (array)your_dress_storage_get('required_plugins')))
			$list[] = array(
					'name' 		=> esc_html__('WooCommerce', 'your-dress'),
					'slug' 		=> 'woocommerce',
					'required' 	=> false
				);

		return $list;
	}
}



// Add WooCommerce specific items into lists
//------------------------------------------------------------------------

// Add sidebar
if ( !function_exists( 'your_dress_woocommerce_list_sidebars' ) ) {
	//Handler of the add_filter( 'your_dress_filter_list_sidebars', 'your_dress_woocommerce_list_sidebars' );
	function your_dress_woocommerce_list_sidebars($list=array()) {
		$list['woocommerce_widgets'] = array(
											'name' => esc_html__('WooCommerce Widgets', 'your-dress'),
											'description' => esc_html__('Widgets to be shown on the WooCommerce pages', 'your-dress')
											);
		return $list;
	}
}




// Decorate WooCommerce output: Loop
//------------------------------------------------------------------------

// Before main content
if ( !function_exists( 'your_dress_woocommerce_wrapper_start' ) ) {
	//remove_action( 'woocommerce_before_main_content', 'your_dress_woocommerce_wrapper_start', 10);
	//Handler of the add_action('woocommerce_before_main_content', 'your_dress_woocommerce_wrapper_start', 10);
	function your_dress_woocommerce_wrapper_start() {
		if (is_product() || is_cart() || is_checkout() || is_account_page()) {
			?>
			<article class="post_item_single post_type_product">
			<?php
		} else {
			?>
			<div class="list_products shop_mode_<?php echo !your_dress_storage_empty('shop_mode') ? your_dress_storage_get('shop_mode') : 'thumbs'; ?>">
				<div class="list_products_header">
			<?php
		}
	}
}

// After main content
if ( !function_exists( 'your_dress_woocommerce_wrapper_end' ) ) {
	//remove_action( 'woocommerce_after_main_content', 'your_dress_woocommerce_wrapper_end', 10);		
	//Handler of the add_action('woocommerce_after_main_content', 'your_dress_woocommerce_wrapper_end', 10);
	function your_dress_woocommerce_wrapper_end() {
		if (is_product() || is_cart() || is_checkout() || is_account_page()) {
			?>
			</article><!-- /.post_item_single -->
			<?php
		} else {
			?>
			</div><!-- /.list_products -->
			<?php
		}
	}
}

// Close header section
if ( !function_exists( 'your_dress_woocommerce_archive_description' ) ) {
	//Handler of the add_action( 'woocommerce_archive_description', 'your_dress_woocommerce_archive_description', 15 );
	function your_dress_woocommerce_archive_description() {
		?>
		</div><!-- /.list_products_header -->
		<?php
	}
}

// Add list mode buttons
if ( !function_exists( 'your_dress_woocommerce_before_shop_loop' ) ) {
	//Handler of the add_action( 'woocommerce_before_shop_loop', 'your_dress_woocommerce_before_shop_loop', 10 );
	function your_dress_woocommerce_before_shop_loop() {
		?>
		<div class="your_dress_shop_mode_buttons"><form action="<?php echo esc_url(your_dress_get_current_url()); ?>" method="post"><input type="hidden" name="your_dress_shop_mode" value="<?php echo esc_attr(your_dress_storage_get('shop_mode')); ?>" /><a href="#" class="woocommerce_thumbs icon-th" title="<?php esc_attr_e('Show products as thumbs', 'your-dress'); ?>"></a><a href="#" class="woocommerce_list icon-th-list" title="<?php esc_attr_e('Show products as list', 'your-dress'); ?>"></a></form></div><!-- /.your_dress_shop_mode_buttons -->
		<?php
	}
}

// Number of columns for the shop streampage
if ( !function_exists( 'your_dress_woocommerce_loop_shop_columns' ) ) {
	//Handler of the add_filter( 'loop_shop_columns', 'your_dress_woocommerce_loop_shop_columns' );
	function your_dress_woocommerce_loop_shop_columns($cols) {
		return max(2, min(4, your_dress_get_theme_option('blog_columns')));
	}
}

// Add column class into product item in shop streampage
if ( !function_exists( 'your_dress_woocommerce_loop_shop_columns_class' ) ) {
	//Handler of the add_filter( 'post_class', 'your_dress_woocommerce_loop_shop_columns_class' );
	//Handler of the add_filter( 'product_cat_class', 'your_dress_woocommerce_loop_shop_columns_class', 10, 3 );
	function your_dress_woocommerce_loop_shop_columns_class($classes, $class='', $cat='') {
		global $woocommerce_loop;
		if (is_product()) {
			if (!empty($woocommerce_loop['columns'])) {
				$classes[] = ' column-1_'.esc_attr($woocommerce_loop['columns']);
			}
		} else if (is_shop() || is_product_category() || is_product_tag() || is_product_taxonomy()) {
			$classes[] = ' column-1_'.esc_attr(max(2, min(4, your_dress_get_theme_option('blog_columns'))));
		}
		return $classes;
	}
}


// Open item wrapper for categories and products
if ( !function_exists( 'your_dress_woocommerce_item_wrapper_start' ) ) {
	//Handler of the add_action( 'woocommerce_before_subcategory_title', 'your_dress_woocommerce_item_wrapper_start', 9 );
	//Handler of the add_action( 'woocommerce_before_shop_loop_item_title', 'your_dress_woocommerce_item_wrapper_start', 9 );
	function your_dress_woocommerce_item_wrapper_start($cat='') {
		your_dress_storage_set('in_product_item', true);
//		$hover = your_dress_get_theme_option('shop_hover');
		$hover = !empty($cat) ? 'icon' : your_dress_get_theme_option('shop_hover');
		?>
		<div class="post_item post_layout_<?php echo esc_attr(your_dress_storage_get('shop_mode')); ?>">
			<div class="post_featured hover_<?php echo esc_attr($hover); ?>">
				<?php do_action('your_dress_action_woocommerce_item_featured_start'); ?>
				<a href="<?php echo esc_url(is_object($cat) ? get_term_link($cat->slug, 'product_cat') : get_permalink()); ?>">
		<?php
	}
}

// Open item wrapper for categories and products
if ( !function_exists( 'your_dress_woocommerce_open_item_wrapper' ) ) {
	//Handler of the add_action( 'woocommerce_before_subcategory_title', 'your_dress_woocommerce_title_wrapper_start', 20 );
	//Handler of the add_action( 'woocommerce_before_shop_loop_item_title', 'your_dress_woocommerce_title_wrapper_start', 20 );
	function your_dress_woocommerce_title_wrapper_start($cat='') {
				?>
				</a>
				<?php
				if (($hover = your_dress_get_theme_option('shop_hover')) != 'none') {
					?><div class="mask"></div><?php
					your_dress_hovers_add_icons($hover, array('cat'=>$cat));
				}
				do_action('your_dress_action_woocommerce_item_featured_end');
				?>
			</div><!-- /.post_featured -->
			<div class="post_data">
				<div class="post_header entry-header">
				<?php
	}
}


// Display product's tags before the title
if ( !function_exists( 'your_dress_woocommerce_title_tags' ) ) {
	//Handler of the add_action( 'woocommerce_before_shop_loop_item_title', 'your_dress_woocommerce_title_tags', 30 );
	function your_dress_woocommerce_title_tags() {
	}
}

// Wrap product title into link
if ( !function_exists( 'your_dress_woocommerce_the_title' ) ) {
	//Handler of the add_filter( 'the_title', 'your_dress_woocommerce_the_title' );
	function your_dress_woocommerce_the_title($title) {
		if (your_dress_storage_get('in_product_item') && get_post_type()=='product') {
			$title = '<a href="'.get_permalink().'">'.esc_html($title).'</a>';
		}
		return $title;
	}
}


// Add excerpt in output for the product in the list mode
if ( !function_exists( 'your_dress_woocommerce_title_wrapper_end' ) ) {
	//Handler of the add_action( 'woocommerce_after_shop_loop_item_title', 'your_dress_woocommerce_title_wrapper_end', 7);
	function your_dress_woocommerce_title_wrapper_end() {
			?>
			</div><!-- /.post_header -->
		<?php

        $price = '';
        global $product;
        if ($product->is_type( 'variable' ) && (!$product->get_variation_price('min') || $product->get_variation_price('min') !== $product->get_variation_price('max')) )
        	$price .= '<span class="from">' . _x('From', 'min_price', 'your-dress') . ' </span>';
        $price .= wc_price($product->get_price());
        ?><span class="price"><?php
        your_dress_show_layout($price);
        ?></span><?php
		if (your_dress_storage_get('shop_mode') == 'list' && (is_shop() || is_product_category() || is_product_tag() || is_product_taxonomy()) && !is_product()) {
		    $excerpt = apply_filters('the_excerpt', get_the_excerpt());
			?>
			<div class="post_content entry-content"><?php your_dress_show_layout($excerpt); ?></div>
			<?php
		}
	}
}

// Add excerpt in output for the product in the list mode
if ( !function_exists( 'your_dress_woocommerce_title_wrapper_end2' ) ) {
	//Handler of the add_action( 'woocommerce_after_subcategory_title', 'your_dress_woocommerce_title_wrapper_end2', 10 );
	function your_dress_woocommerce_title_wrapper_end2($category) {
		?>
			</div><!-- /.post_header -->
		<?php
		if (your_dress_storage_get('shop_mode') == 'list' && is_shop() && !is_product()) {
			?>
			<div class="post_content entry-content"><?php your_dress_show_layout($category->description); ?></div><!-- /.post_content -->
			<?php
		}
	}
}

// Close item wrapper for categories and products
if ( !function_exists( 'your_dress_woocommerce_close_item_wrapper' ) ) {
	//Handler of the add_action( 'woocommerce_after_subcategory', 'your_dress_woocommerce_item_wrapper_end', 20 );
	//Handler of the add_action( 'woocommerce_after_shop_loop_item', 'your_dress_woocommerce_item_wrapper_end', 20 );
	function your_dress_woocommerce_item_wrapper_end($cat='') {
		?>
			</div><!-- /.post_data -->
		</div><!-- /.post_item -->
		<?php
		your_dress_storage_set('in_product_item', false);
	}
}

// Decorate price
if ( !function_exists( 'your_dress_woocommerce_get_price_html' ) ) {
	//Handler of the add_filter(    'woocommerce_get_price_html',	'your_dress_woocommerce_get_price_html' );
	function your_dress_woocommerce_get_price_html($price='') {
	}
}



// Decorate WooCommerce output: Single product
//------------------------------------------------------------------------

// Add Product ID for the single product
if ( !function_exists( 'your_dress_woocommerce_show_product_id' ) ) {
	//Handler of the add_action( 'woocommerce_product_meta_end', 'your_dress_woocommerce_show_product_id', 10);
	function your_dress_woocommerce_show_product_id() {
		$authors = wp_get_post_terms(get_the_ID(), 'pa_product_author');
		if (is_array($authors) && count($authors)>0) {
			echo '<span class="product_author">'.esc_html__('Author: ', 'your-dress');
			$delim = '';
			foreach ($authors as $author) {
				echo  esc_html($delim) . '<span>' . esc_html($author->name) . '</span>';
				$delim = ', ';
			}
			echo '</span>';
		}
		echo '<span class="product_id">'.esc_html__('Product ID: ', 'your-dress') . '<span>' . get_the_ID() . '</span></span>';
	}
}

// Number columns for the product's thumbnails
if ( !function_exists( 'your_dress_woocommerce_product_thumbnails_columns' ) ) {
	//Handler of the add_filter( 'woocommerce_product_thumbnails_columns', 'your_dress_woocommerce_product_thumbnails_columns' );
	function your_dress_woocommerce_product_thumbnails_columns($cols) {
		return 4;
	}
}

// Set columns number for the related products
if ( !function_exists( 'your_dress_woocommerce_output_related_products_args' ) ) {
	//Handler of the add_filter( 'woocommerce_output_related_products_args', 'your_dress_woocommerce_output_related_products_args' );
	function your_dress_woocommerce_output_related_products_args($args) {
		$args['posts_per_page'] = $args['columns'] = max(2, min(4, your_dress_get_theme_option('related_posts')));
		return $args;
	}
}



// Decorate WooCommerce output: Widgets
//------------------------------------------------------------------------

// Search form
if ( !function_exists( 'your_dress_woocommerce_get_product_search_form' ) ) {
	//Handler of the add_filter( 'get_product_search_form', 'your_dress_woocommerce_get_product_search_form' );
	function your_dress_woocommerce_get_product_search_form($form) {
		return '
		<form role="search" method="get" class="search_form" action="' . esc_url(home_url('/')) . '">
			<input type="text" class="search_field" placeholder="' . esc_attr__('Search for products &hellip;', 'your-dress') . '" value="' . get_search_query() . '" name="s" /><button class="search_button" type="submit">' . esc_html__('Search', 'your-dress') . '</button>
			<input type="hidden" name="post_type" value="product" />
		</form>
		';
	}
}





// WooCommerce output: Categories
//------------------------------------------------------------------------

if ( !function_exists( 'your_dress_filter_product_cat_class' ) ) {
	//Handler of the add_filter( 'product_cat_class', 'your_dress_filter_product_cat_class', 10, 3 );
	function your_dress_filter_product_cat_class($classes, $class, $category) {
		if ( in_array( 'first', $classes) ) {
//			$GLOBALS['trx_wc_product_cat_class_counter']
//			array_push($classes, $GLOBALS['trx_wc_product_cat_class_counter']);
		}

		return $classes;
	}
}

// Show subcategory thumbnails.
if ( !function_exists( 'your_dress_woocommerce_subcategory_thumbnail' ) ) {
	//Handler of the add_filter( 'woocommerce_before_subcategory_title', 'your_dress_woocommerce_subcategory_thumbnail', 10 );
	function your_dress_woocommerce_subcategory_thumbnail($category) {
		$small_thumbnail_size = apply_filters( 'subcategory_archive_thumbnail_size', 'woocommerce_thumbnail' );
		$meta = get_term_meta( $category->term_id );
		$thumbnail_id = $meta['thumbnail_id'][0];
		if ( $thumbnail_id ) {
			$image        = wp_get_attachment_image_src( $thumbnail_id, $small_thumbnail_size );
			$image        = $image[0];
		} else {
			$image        = wc_placeholder_img_src();
		}
		if ( $image ) {
			// Prevent esc_url from breaking spaces in urls for image embeds.
			// Ref: https://core.trac.wordpress.org/ticket/23605.
			$image = str_replace( ' ', '%20', $image );
			// Add responsive image markup if available.
			echo '<div style="background-image:url(' . esc_url( $image ) . ')" alt="' . esc_attr( $category->name ) . '" class="your_dress_wc_category_thumbnail"></div>';
		}
	}
}


// Wrap category title into link and add description
if ( !function_exists( 'your_dress_woocommerce_shop_loop_subcategory_title' ) ) {
	//Handler of the add_filter( 'woocommerce_shop_loop_subcategory_title', 'your_dress_woocommerce_shop_loop_subcategory_title' );
	function your_dress_woocommerce_shop_loop_subcategory_title($cat) {
		$link = esc_url(get_term_link($cat->slug, 'product_cat'));
		$cat->name = sprintf('<a href="%s">%s</a>', $link, $cat->name);
		?>
		<h4 class="sc_services_item_title">
		<?php
		echo trim($cat->name);
		if ( $cat->count > 0 ) {
			echo apply_filters( 'woocommerce_subcategory_count_html', '', $cat ); // WPCS: XSS ok.
		}
		?>
		</h4>
		<p class="woocommerce-loop-category__description">
			<?php
			if ( !empty($cat->description) ) {
				echo apply_filters( 'woocommerce_subcategory_description_html', $cat->description, $cat ); // WPCS: XSS ok.
			}
			?>
		</p>
		<div class="sc_services_item_button sc_item_button">
			<a href="<?php echo trim($link); ?>" class="sc_button sc_button_simple">
				<?php _e('Learn more', 'your-dress'); ?>
			</a>
		</div>
		<?php
	}
}




// Add plugin-specific colors and fonts to the custom CSS
if (your_dress_exists_woocommerce()) { require_once YOUR_DRESS_THEME_DIR . 'plugins/woocommerce/woocommerce.styles.php'; }
?>