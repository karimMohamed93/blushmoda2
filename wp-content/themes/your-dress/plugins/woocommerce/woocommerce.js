/* global jQuery:false */
/* global YOUR_DRESS_STORAGE:false */
/* global TRX_ADDONS_STORAGE:false */

(function() {
	"use strict";

	jQuery(document).on('action.init_shortcodes', your_dress_trx_addons_init);
	jQuery(document).on('action.init_hidden_elements', your_dress_trx_addons_init);
	
	
	function your_dress_trx_addons_init(e, container) {
		if (arguments.length < 2) var container = jQuery('body');
		if (container===undefined || container.length === undefined || container.length == 0) return;
		container.find('.sc_countdown_item canvas:not(.inited)').addClass('inited').attr('data-color', YOUR_DRESS_STORAGE['alter_link_color']);
	}

})();