<?php
/* WC Secondary Product Thumbnail support functions
------------------------------------------------------------------------------- */

// Theme init priorities:
// 9 - register other filters (for installer, etc.)
if (!function_exists('your_dress_WPSPT_theme_setup9')) {
	add_action( 'after_setup_theme', 'your_dress_WPSPT_theme_setup9', 9 );
	function your_dress_WPSPT_theme_setup9() {
		if (is_admin()) {
			add_filter( 'your_dress_filter_tgmpa_required_plugins',			'your_dress_WPSPT_tgmpa_required_plugins' );
		}
	}
}

// Filter to add in the required plugins list
if ( !function_exists( 'your_dress_WPSPT_tgmpa_required_plugins' ) ) {
	//Handler of the add_filter('your_dress_filter_tgmpa_required_plugins',	'your_dress_WPSPT_tgmpa_required_plugins');
	function your_dress_WPSPT_tgmpa_required_plugins($list=array()) {
		if (in_array('wc-secondary-product-thumbnail', (array)your_dress_storage_get('required_plugins'))) {
			// WC Secondary Product Thumbnail plugin
			$list[] = array(
					'name' 		=> esc_html__('WC Secondary Product Thumbnail', 'your-dress'),
					'slug' 		=> 'wc-secondary-product-thumbnail',
					'required' 	=> false
			);

		}
		return $list;
	}
}
?>