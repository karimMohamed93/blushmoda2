<?php
/* YITH WooCommerce Wishlist support functions
------------------------------------------------------------------------------- */

if (!function_exists('your_dress_yith_woocommerce_wishlist_theme_setup9')) {
	add_action('after_setup_theme', 'your_dress_yith_woocommerce_wishlist_theme_setup9', 9);
	function your_dress_yith_woocommerce_wishlist_theme_setup9() {
		if (is_admin()) {
			add_filter( 'your_dress_filter_tgmpa_required_plugins',		'your_dress_yith_woocommerce_wishlist_tgmpa_required_plugins' );
		}
	}
}

if (!function_exists('your_yith_woocommerce_wishlist_booking_exists')) {
	function your_yith_woocommerce_wishlist_booking_exists() {
		return class_exists('WCEB_Product_Archive_View');
	}
}


// Filter to add in the required plugins list
if ( !function_exists( 'your_dress_yith_woocommerce_wishlist_tgmpa_required_plugins' ) ) {
	//Handler of the add_filter('your_dress_filter_tgmpa_required_plugins',	'your_dress_yith_woocommerce_wishlist_tgmpa_required_plugins');
	function your_dress_yith_woocommerce_wishlist_tgmpa_required_plugins($list=array()) {
		if (in_array('yith-woocommerce-wishlist', (array)your_dress_storage_get('required_plugins'))) {
			// woocommerce-easy-booking-system plugin
			$list[] = array(
				'name' 		=> esc_html__('YITH WooCommerce Wishlist', 'your-dress'),
				'slug' 		=> 'yith-woocommerce-wishlist',
				'required' 	=> false
			);

		}
		return $list;
	}
}
