<?php
// Add plugin-specific colors and fonts to the custom CSS
if ( !function_exists( 'your_dress_vc_get_css' ) ) {
	add_filter( 'your_dress_filter_get_css', 'your_dress_vc_get_css', 10, 4 );
	function your_dress_vc_get_css($css, $colors, $fonts, $scheme='') {
		if (isset($css['fonts']) && $fonts) {
			$css['fonts'] .= <<<CSS
.vc_tta.vc_tta-accordion .vc_tta-panel-title .vc_tta-title-text {
	{$fonts['p_font-family']}
}
.vc_progress_bar.vc_progress_bar_narrow .vc_single_bar .vc_label .vc_label_units {
	{$fonts['info_font-family']}
}

CSS;
		}

		if (isset($css['colors']) && $colors) {
			$colors['inverse_text_02'] = isset($colors['inverse_text_02']) ? $colors['inverse_text_02'] : '';
			$css['colors'] .= <<<CSS

/* Row and columns */
.scheme_self.vc_section,
.scheme_self.wpb_row,
.scheme_self.wpb_column > .vc_column-inner > .wpb_wrapper,
.scheme_self.wpb_text_column {
	color: {$colors['text']};
}
.scheme_self.vc_section[data-vc-full-width="true"],
.scheme_self.wpb_row[data-vc-full-width="true"],
.scheme_self.wpb_column > .vc_column-inner > .wpb_wrapper,
.scheme_self.wpb_text_column {
	background-color: {$colors['bg_color_0']};
}
.scheme_self.vc_row.vc_parallax[class*="scheme_"] .vc_parallax-inner:before {
	background-color: {$colors['bg_color_08']};
}

/* Accordion */
.wpb-js-composer .vc_tta.vc_tta-accordion .vc_tta-panel-heading .vc_tta-controls-icon {
	color: {$colors['text']};
}
.wpb-js-composer .vc_tta.vc_tta-accordion .vc_tta-panel-heading .vc_tta-controls-icon:before,
.wpb-js-composer .vc_tta.vc_tta-accordion .vc_tta-panel-heading .vc_tta-controls-icon:after {
	border-color: {$colors['inverse_link']};
}
.wpb-js-composer .vc_tta-color-grey.vc_tta-style-classic .vc_tta-panel .vc_tta-panel-title > a {
	color: {$colors['alter_dark']};
}
.wpb-js-composer .vc_tta.vc_tta-accordion .vc_tta-panel+.vc_tta-panel{
     border-color: {$colors['input_bg_color']};
}
.wpb-js-composer .vc_tta-color-grey.vc_tta-style-classic .vc_tta-panel.vc_active .vc_tta-panel-title > a,
.wpb-js-composer .vc_tta-color-grey.vc_tta-style-classic .vc_tta-panel .vc_tta-panel-title > a:hover {
	color: {$colors['alter_dark']};
}
.wpb-js-composer .vc_tta-color-grey.vc_tta-style-classic .vc_tta-panel.vc_active .vc_tta-panel-title > a .vc_tta-controls-icon,
.wpb-js-composer .vc_tta-color-grey.vc_tta-style-classic .vc_tta-panel .vc_tta-panel-title > a:hover .vc_tta-controls-icon {
	color: {$colors['text']};
}
.wpb-js-composer .vc_tta-color-grey.vc_tta-style-classic .vc_tta-panel.vc_active .vc_tta-panel-title > a .vc_tta-controls-icon:before,
.wpb-js-composer .vc_tta-color-grey.vc_tta-style-classic .vc_tta-panel.vc_active .vc_tta-panel-title > a .vc_tta-controls-icon:after {
	border-color: {$colors['inverse_text']};
}
 .wpb-js-composer .vc_tta.vc_tta-accordion .vc_tta-panel:hover,
 .wpb-js-composer .vc_tta.vc_tta-accordion .vc_tta-panel.vc_active,
 .wpb-js-composer .vc_tta.vc_tta-accordion .vc_tta-panel.vc_active .vc_tta-panel-body{
    background-color: {$colors['input_bg_color']};
 }
 .wpb-js-composer .vc_tta.vc_tta-accordion.vc_tta-color-white .vc_tta-panel+.vc_tta-panel {
    border-color: {$colors['input_bg_color_01']};
 }
 .wpb-js-composer .vc_tta.vc_tta-accordion.vc_tta-color-white .vc_tta-panel:hover,
 .wpb-js-composer .vc_tta.vc_tta-accordion.vc_tta-color-white .vc_tta-panel.vc_active,
 .wpb-js-composer .vc_tta.vc_tta-accordion.vc_tta-color-white .vc_tta-panel.vc_active .vc_tta-panel-body{
    background-color: {$colors['input_bg_color_01']};
 }
 .wpb-js-composer .vc_tta-color-white.vc_tta-style-classic .vc_tta-panel.vc_active .vc_tta-panel-title>a,
 .wpb-js-composer .vc_tta.vc_tta-accordion.vc_tta-color-white .vc_tta-panel-heading .vc_tta-controls-icon,
 .wpb-js-composer .vc_tta-color-white.vc_tta-style-classic .vc_tta-panel .vc_tta-panel-body,
 .wpb-js-composer .vc_tta-color-white.vc_tta-style-classic .vc_tta-panel .vc_tta-panel-title>a {
    color: {$colors['inverse_text']};
 }
/* Tabs */
.wpb-js-composer .vc_tta-color-grey.vc_tta-style-classic .vc_tta-tabs-list .vc_tta-tab > a {
	color: {$colors['alter_hover']};
	background-color: {$colors['bg_color_0']};
}
.wpb-js-composer .vc_tta-color-grey.vc_tta-style-classic .vc_tta-tabs-list .vc_tta-tab > a:hover,
.wpb-js-composer .vc_tta-color-grey.vc_tta-style-classic .vc_tta-tabs-list .vc_tta-tab.vc_active > a {
	color: {$colors['inverse_text']};
	background-color: {$colors['text_hover']};
}
.wpb-js-composer .vc_tta-tab:not(.vc_active) + .vc_tta-tab:before{
    background-color:  {$colors['text_hover']};
}

/* Separator */
.wpb-js-composer .vc_separator.vc_sep_color_grey .vc_sep_line {
	border-color: {$colors['bd_color']};
}

/* Progress bar */
.wpb-js-composer .vc_progress_bar.vc_progress_bar_narrow .vc_single_bar {
    color:  {$colors['alter_dark']};
}
.wpb-js-composer .vc_progress_bar.vc_progress-bar-color-white .vc_single_bar {
	background-color: {$colors['inverse_text_02']};
}
.wpb-js-composer .vc_progress_bar.vc_progress_bar_narrow.vc_progress-bar-color-bar_red .vc_single_bar .vc_bar {
	background-color: {$colors['alter_link']};
}
.wpb-js-composer .vc_progress_bar.vc_progress-bar-color-white .vc_single_bar .vc_label {
	color: {$colors['inverse_text']};
}
.vc_progress_bar.vc_progress_bar_narrow .vc_single_bar .vc_label{
    color:  {$colors['alter_dark']};
}



.vc_message_box.vc_message_box_closeable:after {
    color: {$colors['inverse_text']};
    background-color: {$colors['bg_color_02']};
}
.vc_message_box .vc_message_box-icon i{
    color: {$colors['bg_color_07']};
}


.vc_color-warning.vc_message_box {
    background-color: {$colors['text_link']};
    color: {$colors['inverse_text']};
}
.vc_color-info.vc_message_box {
    background-color: {$colors['inverse_hover']};
    color: {$colors['inverse_text']};
}

.vc_color-success.vc_message_box {
    background-color: {$colors['inverse_link']};
    color: {$colors['inverse_text']};
}
.vc_color-grey.vc_message_box {
    background-color: {$colors['input_bg_color']};
    color: {$colors['alter_dark']};
}
.vc_color-grey.vc_message_box.vc_message_box_closeable:after {
    color: {$colors['bg_color']};
    background-color: {$colors['input_bd_hover']};
}
.vc_color-grey.vc_message_box .vc_message_box-icon i{
    color: {$colors['alter_dark']};
}
CSS;
		}
		
		return $css;
	}
}
?>