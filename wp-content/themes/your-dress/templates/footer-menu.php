<?php
/**
 * The template to display menu in the footer
 *
 * @package WordPress
 * @subpackage YOUR_DRESS
 * @since YOUR_DRESS 1.0.10
 */

// Footer menu
$your_dress_menu_footer = your_dress_get_nav_menu('menu_footer');
if (!empty($your_dress_menu_footer)) {
	?>
	<div class="footer_menu_wrap">
		<div class="footer_menu_inner">
			<?php your_dress_show_layout($your_dress_menu_footer); ?>
		</div>
	</div>
	<?php
}
?>