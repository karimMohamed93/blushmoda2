<?php
/**
 * The template to display the widgets area in the header
 *
 * @package WordPress
 * @subpackage YOUR_DRESS
 * @since YOUR_DRESS 1.0
 */

// Header sidebar
$your_dress_header_name = your_dress_get_theme_option('header_widgets');
$your_dress_header_present = !your_dress_is_off($your_dress_header_name) && is_active_sidebar($your_dress_header_name);
if ($your_dress_header_present) { 
	your_dress_storage_set('current_sidebar', 'header');
	$your_dress_header_wide = your_dress_get_theme_option('header_wide');
	ob_start();
	if ( is_active_sidebar($your_dress_header_name) ) {
		dynamic_sidebar($your_dress_header_name);
	}
	$your_dress_widgets_output = ob_get_contents();
	ob_end_clean();
	if (!empty($your_dress_widgets_output)) {
		$your_dress_widgets_output = preg_replace("/<\/aside>[\r\n\s]*<aside/", "</aside><aside", $your_dress_widgets_output);
		$your_dress_need_columns = strpos($your_dress_widgets_output, 'columns_wrap')===false;
		if ($your_dress_need_columns) {
			$your_dress_columns = max(0, (int) your_dress_get_theme_option('header_columns'));
			if ($your_dress_columns == 0) $your_dress_columns = min(6, max(1, substr_count($your_dress_widgets_output, '<aside ')));
			if ($your_dress_columns > 1)
				$your_dress_widgets_output = preg_replace("/class=\"widget /", "class=\"column-1_".esc_attr($your_dress_columns).' widget ', $your_dress_widgets_output);
			else
				$your_dress_need_columns = false;
		}
		?>
		<div class="header_widgets_wrap widget_area<?php echo !empty($your_dress_header_wide) ? ' header_fullwidth' : ' header_boxed'; ?>">
			<div class="header_widgets_inner widget_area_inner">
				<?php 
				if (!$your_dress_header_wide) { 
					?><div class="content_wrap"><?php
				}
				if ($your_dress_need_columns) {
					?><div class="columns_wrap"><?php
				}
				do_action( 'your_dress_action_before_sidebar' );
				your_dress_show_layout($your_dress_widgets_output);
				do_action( 'your_dress_action_after_sidebar' );
				if ($your_dress_need_columns) {
					?></div>	<!-- /.columns_wrap --><?php
				}
				if (!$your_dress_header_wide) {
					?></div>	<!-- /.content_wrap --><?php
				}
				?>
			</div>	<!-- /.header_widgets_inner -->
		</div>	<!-- /.header_widgets_wrap -->
		<?php
	}
}
?>