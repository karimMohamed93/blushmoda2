<?php
/**
 * The template to display the page title and breadcrumbs
 *
 * @package WordPress
 * @subpackage YOUR_DRESS
 * @since YOUR_DRESS 1.0
 */

// Page (category, tag, archive, author) title

if ( your_dress_need_page_title() ) {
	your_dress_sc_layouts_showed('title', true);
	your_dress_sc_layouts_showed('postmeta', true);
	?>
	<div class="top_panel_title sc_layouts_row sc_layouts_row_type_normal">
		<div class="content_wrap">
			<div class="sc_layouts_column sc_layouts_column_align_center">
				<div class="sc_layouts_item">
					<div class="sc_layouts_title">
						<?php
						// Blog/Post title
						?><div class="sc_layouts_title_title"><?php
							$your_dress_blog_title = your_dress_get_blog_title();
							$your_dress_blog_title_text = $your_dress_blog_title_class = $your_dress_blog_title_link = $your_dress_blog_title_link_text = '';
							if (is_array($your_dress_blog_title)) {
								$your_dress_blog_title_text = $your_dress_blog_title['text'];
								$your_dress_blog_title_class = !empty($your_dress_blog_title['class']) ? ' '.$your_dress_blog_title['class'] : '';
								$your_dress_blog_title_link = !empty($your_dress_blog_title['link']) ? $your_dress_blog_title['link'] : '';
								$your_dress_blog_title_link_text = !empty($your_dress_blog_title['link_text']) ? $your_dress_blog_title['link_text'] : '';
							} else
								$your_dress_blog_title_text = $your_dress_blog_title;
							?>
							<h1 class="sc_layouts_title_caption<?php echo esc_attr($your_dress_blog_title_class); ?>"><?php
								$your_dress_top_icon = your_dress_get_category_icon();
								if (!empty($your_dress_top_icon)) {
									$your_dress_attr = your_dress_getimagesize($your_dress_top_icon);
									?><img src="<?php echo esc_url($your_dress_top_icon); ?>"  <?php if (!empty($your_dress_attr[3])) your_dress_show_layout($your_dress_attr[3]);?>><?php
								}
								echo wp_kses_data($your_dress_blog_title_text);
							?></h1>
							<?php
							if (!empty($your_dress_blog_title_link) && !empty($your_dress_blog_title_link_text)) {
								?><a href="<?php echo esc_url($your_dress_blog_title_link); ?>" class="theme_button theme_button_small sc_layouts_title_link"><?php echo esc_html($your_dress_blog_title_link_text); ?></a><?php
							}
							
							// Category/Tag description
							if ( is_category() || is_tag() || is_tax() ) 
								the_archive_description( '<div class="sc_layouts_title_description">', '</div>' );
		
						?></div><?php
	
						// Breadcrumbs
						?><div class="sc_layouts_title_breadcrumbs"><?php
							do_action( 'your_dress_action_breadcrumbs');
						?></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php
}
?>