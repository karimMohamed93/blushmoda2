<?php
/**
 * The template to display the Author bio
 *
 * @package WordPress
 * @subpackage YOUR_DRESS
 * @since YOUR_DRESS 1.0
 */
?>

<div class="author_info author vcard" itemprop="author" itemscope itemtype="http://schema.org/Person">

	<div class="author_avatar" itemprop="image">
		<?php 
		$your_dress_mult = your_dress_get_retina_multiplier();
		echo get_avatar( get_the_author_meta( 'user_email' ), 120*$your_dress_mult ); 
		?>
	</div><!-- .author_avatar -->

	<div class="author_description"><?php esc_html_e( 'About author', 'your-dress' ); ?>
		<h5 class="author_title" itemprop="name"><?php echo wp_kses_data(sprintf('<span class="fn">'.get_the_author().'</span>')); ?></h5>

		<div class="author_bio" itemprop="description">
			<?php echo wp_kses_post(wpautop(get_the_author_meta( 'description' ))); ?>
			<?php do_action('your_dress_action_user_meta'); ?>
		</div><!-- .author_bio -->

	</div><!-- .author_description -->

</div><!-- .author_info -->
