<?php
/**
 * The template 'Style 1' to displaying related posts
 *
 * @package WordPress
 * @subpackage YOUR_DRESS
 * @since YOUR_DRESS 1.0
 */

$your_dress_link = get_permalink();
$your_dress_post_format = get_post_format();
$your_dress_post_format = empty($your_dress_post_format) ? 'standard' : str_replace('post-format-', '', $your_dress_post_format);
?><div id="post-<?php the_ID(); ?>" 
	<?php post_class( 'related_item related_item_style_1 post_format_'.esc_attr($your_dress_post_format) ); ?>><?php
	your_dress_show_post_featured(array(
		'thumb_size' => your_dress_get_thumb_size( 'big' ),
		'show_no_image' => false,
		'singular' => false,
		'post_info' => '<div class="post_header entry-header">'
							. '<div class="post_categories">' . your_dress_get_post_categories('') . '</div>'
							. '<h6 class="post_title entry-title"><a href="' . esc_url($your_dress_link) . '">' . get_the_title() . '</a></h6>'
							. (in_array(get_post_type(), array('post', 'attachment'))
									? '<span class="post_date"><a href="' . esc_url($your_dress_link) . '">' . your_dress_get_date() . '</a></span>'
									: '')
						. '</div>'
		)
	);
?></div>