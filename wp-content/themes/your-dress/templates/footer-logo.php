<?php
/**
 * The template to display the site logo in the footer
 *
 * @package WordPress
 * @subpackage YOUR_DRESS
 * @since YOUR_DRESS 1.0.10
 */

// Logo
if (your_dress_is_on(your_dress_get_theme_option('logo_in_footer'))) {
	$your_dress_logo_image = '';
	if (your_dress_get_retina_multiplier(2) > 1)
		$your_dress_logo_image = your_dress_get_theme_option( 'logo_footer_retina' );
	if (empty($your_dress_logo_image)) 
		$your_dress_logo_image = your_dress_get_theme_option( 'logo_footer' );
	$your_dress_logo_text   = get_bloginfo( 'name' );
	if (!empty($your_dress_logo_image) || !empty($your_dress_logo_text)) {
		?>
		<div class="footer_logo_wrap">
			<div class="footer_logo_inner">
				<?php
				if (!empty($your_dress_logo_image)) {
					$your_dress_attr = your_dress_getimagesize($your_dress_logo_image);
					echo '<a href="'.esc_url(home_url('/')).'"><img src="'.esc_url($your_dress_logo_image).'" class="logo_footer_image" '.(!empty($your_dress_attr[3]) ? sprintf(' %s', $your_dress_attr[3]) : '').'></a>' ;
				} else if (!empty($your_dress_logo_text)) {
					echo '<h1 class="logo_footer_text"><a href="'.esc_url(home_url('/')).'">' . esc_html($your_dress_logo_text) . '</a></h1>';
				}
				?>
			</div>
		</div>
		<?php
	}
}
?>