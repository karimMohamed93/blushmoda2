<?php
/**
 * The template to display the socials in the footer
 *
 * @package WordPress
 * @subpackage YOUR_DRESS
 * @since YOUR_DRESS 1.0.10
 */


// Socials
if ( your_dress_is_on(your_dress_get_theme_option('socials_in_footer')) && ($your_dress_output = your_dress_get_socials_links()) != '') {
	?>
	<div class="footer_socials_wrap socials_wrap">
		<div class="footer_socials_inner">
			<?php your_dress_show_layout($your_dress_output); ?>
		</div>
	</div>
	<?php
}
?>