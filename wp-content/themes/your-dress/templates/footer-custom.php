<?php
/**
 * The template to display default site footer
 *
 * @package WordPress
 * @subpackage YOUR_DRESS
 * @since YOUR_DRESS 1.0.10
 */

$your_dress_footer_scheme =  your_dress_is_inherit(your_dress_get_theme_option('footer_scheme')) ? your_dress_get_theme_option('color_scheme') : your_dress_get_theme_option('footer_scheme');
$your_dress_footer_id = str_replace('footer-custom-', '', your_dress_get_theme_option("footer_style"));
$your_dress_footer_meta = get_post_meta($your_dress_footer_id, 'trx_addons_options', true);
?>
<footer class="footer_wrap footer_custom footer_custom_<?php echo esc_attr($your_dress_footer_id); 
						?> footer_custom_<?php echo esc_attr(sanitize_title(get_the_title($your_dress_footer_id))); 
						if (!empty($your_dress_footer_meta['margin']) != '') 
							echo ' '.esc_attr(your_dress_add_inline_css_class('margin-top: '.esc_attr(your_dress_prepare_css_value($your_dress_footer_meta['margin'])).';'));
						?> scheme_<?php echo esc_attr($your_dress_footer_scheme); 
						?>">
	<?php
    // Custom footer's layout
    do_action('your_dress_action_show_layout', $your_dress_footer_id);
	?>
</footer><!-- /.footer_wrap -->
