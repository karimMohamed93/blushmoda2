<?php
/**
 * The template to display the logo or the site name and the slogan in the Header
 *
 * @package WordPress
 * @subpackage YOUR_DRESS
 * @since YOUR_DRESS 1.0
 */

$your_dress_args = get_query_var('your_dress_logo_args');

// Site logo
$your_dress_logo_image  = your_dress_get_logo_image(isset($your_dress_args['type']) ? $your_dress_args['type'] : '');
$your_dress_logo_text   = your_dress_is_on(your_dress_get_theme_option('logo_text')) ? get_bloginfo( 'name' ) : '';
$your_dress_logo_slogan = get_bloginfo( 'description', 'display' );
if (!empty($your_dress_logo_image) || !empty($your_dress_logo_text)) {
	?><a class="sc_layouts_logo" href="<?php echo is_front_page() ? '#' : esc_url(home_url('/')); ?>"><?php
		if (!empty($your_dress_logo_image)) {
			$your_dress_attr = your_dress_getimagesize($your_dress_logo_image);
			echo '<img src="'.esc_url($your_dress_logo_image).'" '.(!empty($your_dress_attr[3]) ? sprintf(' %s', $your_dress_attr[3]) : '').'>' ;
		} else {
			your_dress_show_layout(your_dress_prepare_macros($your_dress_logo_text), '<span class="logo_text">', '</span>');
			your_dress_show_layout(your_dress_prepare_macros($your_dress_logo_slogan), '<span class="logo_slogan">', '</span>');
		}
	?></a><?php
}
?>