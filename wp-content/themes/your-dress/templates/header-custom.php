<?php
/**
 * The template to display custom header from the ThemeREX Addons Layouts
 *
 * @package WordPress
 * @subpackage YOUR_DRESS
 * @since YOUR_DRESS 1.0.06
 */

$your_dress_header_css = $your_dress_header_image = '';
$your_dress_header_video = your_dress_get_header_video();
if (true || empty($your_dress_header_video)) {
	$your_dress_header_image = get_header_image();
	if (your_dress_is_on(your_dress_get_theme_option('header_image_override')) && apply_filters('your_dress_filter_allow_override_header_image', true)) {
		if (is_category()) {
			if (($your_dress_cat_img = your_dress_get_category_image()) != '')
				$your_dress_header_image = $your_dress_cat_img;
		} else if (is_singular() || your_dress_storage_isset('blog_archive')) {
			if (has_post_thumbnail()) {
				$your_dress_header_image = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' );
				if (is_array($your_dress_header_image)) $your_dress_header_image = $your_dress_header_image[0];
			} else
				$your_dress_header_image = '';
		}
	}
}

$your_dress_header_id = str_replace('header-custom-', '', your_dress_get_theme_option("header_style"));
$your_dress_header_meta = get_post_meta($your_dress_header_id, 'trx_addons_options', true);

?><header class="top_panel top_panel_custom top_panel_custom_<?php echo esc_attr($your_dress_header_id); 
						?> top_panel_custom_<?php echo esc_attr(sanitize_title(get_the_title($your_dress_header_id)));
						echo !empty($your_dress_header_image) || !empty($your_dress_header_video) 
							? ' with_bg_image' 
							: ' without_bg_image';
						if ($your_dress_header_video!='') 
							echo ' with_bg_video';
						if ($your_dress_header_image!='') 
							echo ' '.esc_attr(your_dress_add_inline_css_class('background-image: url('.esc_url($your_dress_header_image).');'));
						if (!empty($your_dress_header_meta['margin']) != '') 
							echo ' '.esc_attr(your_dress_add_inline_css_class('margin-bottom: '.esc_attr(your_dress_prepare_css_value($your_dress_header_meta['margin'])).';'));
						if (is_single() && has_post_thumbnail()) 
							echo ' with_featured_image';
						if (your_dress_is_on(your_dress_get_theme_option('header_fullheight'))) 
							echo ' header_fullheight trx-stretch-height';
						?> scheme_<?php echo esc_attr(your_dress_is_inherit(your_dress_get_theme_option('header_scheme')) 
														? your_dress_get_theme_option('color_scheme') 
														: your_dress_get_theme_option('header_scheme'));
						?>"><?php

	// Background video
	if (!empty($your_dress_header_video)) {
		get_template_part( 'templates/header-video' );
	}
		
	// Custom header's layout
	do_action('your_dress_action_show_layout', $your_dress_header_id);

	// Header widgets area
	get_template_part( 'templates/header-widgets' );
		
?></header>