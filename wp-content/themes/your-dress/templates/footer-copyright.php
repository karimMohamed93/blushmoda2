<?php
/**
 * The template to display the copyright info in the footer
 *
 * @package WordPress
 * @subpackage YOUR_DRESS
 * @since YOUR_DRESS 1.0.10
 */

// Copyright area
$your_dress_footer_scheme =  your_dress_is_inherit(your_dress_get_theme_option('footer_scheme')) ? your_dress_get_theme_option('color_scheme') : your_dress_get_theme_option('footer_scheme');
$your_dress_copyright_scheme = your_dress_is_inherit(your_dress_get_theme_option('copyright_scheme')) ? $your_dress_footer_scheme : your_dress_get_theme_option('copyright_scheme');
?> 
<div class="footer_copyright_wrap scheme_<?php echo esc_attr($your_dress_copyright_scheme); ?>">
	<div class="footer_copyright_inner">
		<div class="content_wrap">
			<div class="copyright_text"><?php
				// Replace {{...}} and [[...]] on the <i>...</i> and <b>...</b>
				$your_dress_copyright = your_dress_prepare_macros(your_dress_get_theme_option('copyright'));
				if (!empty($your_dress_copyright)) {
					// Replace {date_format} on the current date in the specified format
					if (preg_match("/(\\{[\\w\\d\\\\\\-\\:]*\\})/", $your_dress_copyright, $your_dress_matches)) {
						$your_dress_copyright = str_replace($your_dress_matches[1], date(str_replace(array('{', '}'), '', $your_dress_matches[1])), $your_dress_copyright);
					}
					$your_dress_copyright = str_replace(array('{{Y}}', '{Y}'), date('Y'), $your_dress_copyright);
					// Display copyright
					echo wp_kses_data(nl2br($your_dress_copyright));
				}
			?></div>
		</div>
	</div>
</div>
