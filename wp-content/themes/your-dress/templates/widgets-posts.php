<?php
/**
 * The template to display posts in widgets and/or in the search results
 *
 * @package WordPress
 * @subpackage YOUR_DRESS
 * @since YOUR_DRESS 1.0
 */

$your_dress_post_id    = get_the_ID();
$your_dress_post_date  = your_dress_get_date();
$your_dress_post_title = get_the_title();
$your_dress_post_link  = get_permalink();
$your_dress_post_author_id   = get_the_author_meta('ID');
$your_dress_post_author_name = get_the_author_meta('display_name');
$your_dress_post_author_url  = get_author_posts_url($your_dress_post_author_id, '');

$your_dress_args = get_query_var('your_dress_args_widgets_posts');
$your_dress_show_date = isset($your_dress_args['show_date']) ? (int) $your_dress_args['show_date'] : 1;
$your_dress_show_image = isset($your_dress_args['show_image']) ? (int) $your_dress_args['show_image'] : 1;
$your_dress_show_author = isset($your_dress_args['show_author']) ? (int) $your_dress_args['show_author'] : 1;
$your_dress_show_counters = isset($your_dress_args['show_counters']) ? (int) $your_dress_args['show_counters'] : 1;
$your_dress_show_categories = isset($your_dress_args['show_categories']) ? (int) $your_dress_args['show_categories'] : 1;

$your_dress_output = your_dress_storage_get('your_dress_output_widgets_posts');

$your_dress_post_counters_output = '';
if ( $your_dress_show_counters ) {
	$your_dress_post_counters_output = '<span class="post_info_item post_info_counters">'
								. your_dress_get_post_counters('comments')
							. '</span>';
}


$your_dress_output .= '<article class="post_item with_thumb">';

if ($your_dress_show_image) {
	$your_dress_post_thumb = get_the_post_thumbnail($your_dress_post_id, your_dress_get_thumb_size('tiny'), array(
		'alt' => get_the_title()
	));
	if ($your_dress_post_thumb) $your_dress_output .= '<div class="post_thumb">' . ($your_dress_post_link ? '<a href="' . esc_url($your_dress_post_link) . '">' : '') . ($your_dress_post_thumb) . ($your_dress_post_link ? '</a>' : '') . '</div>';
}

$your_dress_output .= '<div class="post_content">'
			. ($your_dress_show_categories 
					? '<div class="post_categories">'
						. your_dress_get_post_categories()
						. $your_dress_post_counters_output
						. '</div>' 
					: '')
			. '<h6 class="post_title">' . ($your_dress_post_link ? '<a href="' . esc_url($your_dress_post_link) . '">' : '') . ($your_dress_post_title) . ($your_dress_post_link ? '</a>' : '') . '</h6>'
			. apply_filters('your_dress_filter_get_post_info', 
								'<div class="post_info">'
									. ($your_dress_show_date 
										? '<span class="post_info_item post_info_posted">'
											. ($your_dress_post_link ? '<a href="' . esc_url($your_dress_post_link) . '" class="post_info_date">' : '') 
											. esc_html($your_dress_post_date) 
											. ($your_dress_post_link ? '</a>' : '')
											. '</span>'
										: '')
									. ($your_dress_show_author 
										? '<span class="post_info_item post_info_posted_by">' 
											. esc_html__('by', 'your-dress') . ' ' 
											. ($your_dress_post_link ? '<a href="' . esc_url($your_dress_post_author_url) . '" class="post_info_author">' : '') 
											. esc_html($your_dress_post_author_name) 
											. ($your_dress_post_link ? '</a>' : '') 
											. '</span>'
										: '')
									. (!$your_dress_show_categories && $your_dress_post_counters_output
										? $your_dress_post_counters_output
										: '')
								. '</div>')
		. '</div>'
	. '</article>';
your_dress_storage_set('your_dress_output_widgets_posts', $your_dress_output);
?>