<?php
/**
 * The Portfolio template to display the content
 *
 * Used for index/archive/search.
 *
 * @package WordPress
 * @subpackage YOUR_DRESS
 * @since YOUR_DRESS 1.0
 */

$your_dress_blog_style = explode('_', your_dress_get_theme_option('blog_style'));
$your_dress_columns = empty($your_dress_blog_style[1]) ? 2 : max(2, $your_dress_blog_style[1]);
$your_dress_post_format = get_post_format();
$your_dress_post_format = empty($your_dress_post_format) ? 'standard' : str_replace('post-format-', '', $your_dress_post_format);
$your_dress_animation = your_dress_get_theme_option('blog_animation');

?><article id="post-<?php the_ID(); ?>" 
	<?php post_class( 'post_item post_layout_portfolio post_layout_portfolio_'.esc_attr($your_dress_columns).' post_format_'.esc_attr($your_dress_post_format) ); ?>
	<?php echo (!your_dress_is_off($your_dress_animation) ? ' data-animation="'.esc_attr(your_dress_get_animation_classes($your_dress_animation)).'"' : ''); ?>
	>

	<?php
	$your_dress_image_hover = your_dress_get_theme_option('image_hover');
	// Featured image
	your_dress_show_post_featured(array(
		'thumb_size' => your_dress_get_thumb_size(strpos(your_dress_get_theme_option('body_style'), 'full')!==false || $your_dress_columns < 3 ? 'masonry-big' : 'masonry'),
		'show_no_image' => true,
		'class' => $your_dress_image_hover == 'dots' ? 'hover_with_info' : '',
		'post_info' => $your_dress_image_hover == 'dots' ? '<div class="post_info">'.esc_html(get_the_title()).'</div>' : ''
	));
	?>
</article>