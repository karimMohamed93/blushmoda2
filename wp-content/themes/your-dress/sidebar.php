<?php
/**
 * The Sidebar containing the main widget areas.
 *
 * @package WordPress
 * @subpackage YOUR_DRESS
 * @since YOUR_DRESS 1.0
 */

$your_dress_sidebar_position = your_dress_get_theme_option('sidebar_position');
if (your_dress_sidebar_present()) {
	ob_start();
	$your_dress_sidebar_name = your_dress_get_theme_option('sidebar_widgets');
	your_dress_storage_set('current_sidebar', 'sidebar');
	if ( is_active_sidebar($your_dress_sidebar_name) ) {
		dynamic_sidebar($your_dress_sidebar_name);
	}
	$your_dress_out = trim(ob_get_contents());
	ob_end_clean();
	if (!empty($your_dress_out)) {
		?>
		<div class="sidebar <?php echo esc_attr($your_dress_sidebar_position); ?> widget_area<?php if (!your_dress_is_inherit(your_dress_get_theme_option('sidebar_scheme'))) echo ' scheme_'.esc_attr(your_dress_get_theme_option('sidebar_scheme')); ?>" role="complementary">
			<div class="sidebar_inner">
				<?php
				do_action( 'your_dress_action_before_sidebar' );
				your_dress_show_layout(preg_replace("/<\/aside>[\r\n\s]*<aside/", "</aside><aside", $your_dress_out));
				do_action( 'your_dress_action_after_sidebar' );
				?>
			</div><!-- /.sidebar_inner -->
		</div><!-- /.sidebar -->
		<?php
	}
}
?>