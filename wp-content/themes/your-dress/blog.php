<?php
/**
 * The template to display blog archive
 *
 * @package WordPress
 * @subpackage YOUR_DRESS
 * @since YOUR_DRESS 1.0
 */

/*
Template Name: Blog archive
*/

/**
 * Make page with this template and put it into menu
 * to display posts as blog archive
 * You can setup output parameters (blog style, posts per page, parent category, etc.)
 * in the Theme Options section (under the page content)
 * You can build this page in the Visual Composer to make custom page layout:
 * just insert %%CONTENT%% in the desired place of content
 */

// Get template page's content
$your_dress_content = '';
$your_dress_blog_archive_mask = '%%CONTENT%%';
$your_dress_blog_archive_subst = sprintf('<div class="blog_archive">%s</div>', $your_dress_blog_archive_mask);
if ( have_posts() ) {
	the_post(); 
	if (($your_dress_content = apply_filters('the_content', get_the_content())) != '') {
		if (($your_dress_pos = strpos($your_dress_content, $your_dress_blog_archive_mask)) !== false) {
			$your_dress_content = preg_replace('/(\<p\>\s*)?'.$your_dress_blog_archive_mask.'(\s*\<\/p\>)/i', $your_dress_blog_archive_subst, $your_dress_content);
		} else
			$your_dress_content .= $your_dress_blog_archive_subst;
		$your_dress_content = explode($your_dress_blog_archive_mask, $your_dress_content);
		// Add VC custom styles to the inline CSS
		$vc_custom_css = get_post_meta( get_the_ID(), '_wpb_shortcodes_custom_css', true );
		if ( !empty( $vc_custom_css ) ) your_dress_add_inline_css(strip_tags($vc_custom_css));
	}
}

// Prepare args for a new query
$your_dress_args = array(
	'post_status' => current_user_can('read_private_pages') && current_user_can('read_private_posts') ? array('publish', 'private') : 'publish'
);
$your_dress_args = your_dress_query_add_posts_and_cats($your_dress_args, '', your_dress_get_theme_option('post_type'), your_dress_get_theme_option('parent_cat'));
$your_dress_page_number = get_query_var('paged') ? get_query_var('paged') : (get_query_var('page') ? get_query_var('page') : 1);
if ($your_dress_page_number > 1) {
	$your_dress_args['paged'] = $your_dress_page_number;
	$your_dress_args['ignore_sticky_posts'] = true;
}
$your_dress_ppp = your_dress_get_theme_option('posts_per_page');
if ((int) $your_dress_ppp != 0)
	$your_dress_args['posts_per_page'] = (int) $your_dress_ppp;
// Make a new query
query_posts( $your_dress_args );
// Set a new query as main WP Query
$GLOBALS['wp_the_query'] = $GLOBALS['wp_query'];

// Set query vars in the new query!
if (is_array($your_dress_content) && count($your_dress_content) == 2) {
	set_query_var('blog_archive_start', $your_dress_content[0]);
	set_query_var('blog_archive_end', $your_dress_content[1]);
}

get_template_part('index');
?>