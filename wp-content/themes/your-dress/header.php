<?php
/**
 * The Header: Logo and main menu
 *
 * @package WordPress
 * @subpackage YOUR_DRESS
 * @since YOUR_DRESS 1.0
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js scheme_<?php
										 // Class scheme_xxx need in the <html> as context for the <body>!
										 echo esc_attr(your_dress_get_theme_option('color_scheme'));
										 ?>">
<head>
	<?php wp_head(); ?>
</head>

<body <?php	body_class(); ?>>

	<?php do_action( 'your_dress_action_before' ); ?>

	<div class="body_wrap">
        <?php
        if (your_dress_get_theme_option('body_style') == 'boxed' && (your_dress_get_theme_option('body_style') != 'show_socials_side') && ($your_dress_output = your_dress_get_socials_links()) != '' ) {
            ?><div class="side_socials <?php
            if(your_dress_get_theme_option('show_socials_pos') == '0') {
                echo 'top-pos';
            }
            ?>"><?php
            your_dress_show_layout($your_dress_output);
            ?></div><?php
        }
        if (your_dress_get_theme_option('body_style') == 'boxed' && (your_dress_get_theme_option('show_link_side') != '') && (your_dress_get_theme_option('show_link_address') != '')) {
            ?><div class="side_link <?php
            if(your_dress_get_theme_option('show_socials_pos') == '0') {
                echo 'top-pos';
            }
            ?>"><?php
               echo '<a href="' . esc_attr(your_dress_get_theme_option('show_link_address')) . '">' . esc_attr(your_dress_get_theme_option('show_link_side')) .'</a>';
            ?></div><?php
        }
        ?>
		<div class="page_wrap">

			<?php
			// Desktop header
			$your_dress_header_style = your_dress_get_theme_option("header_style");
			if (strpos($your_dress_header_style, 'header-custom-')===0) $your_dress_header_style = 'header-custom';
			get_template_part( "templates/{$your_dress_header_style}");

			// Side menu
			if (in_array(your_dress_get_theme_option('menu_style'), array('left', 'right'))) {
				get_template_part( 'templates/header-navi-side' );
			}

			// Mobile header
			get_template_part( 'templates/header-mobile');
			?>

			<div class="page_content_wrap scheme_<?php echo esc_attr(your_dress_get_theme_option('color_scheme')); ?>">

				<?php if (your_dress_get_theme_option('body_style') != 'fullscreen') { ?>
				<div class="content_wrap">
				<?php } ?>

					<?php
					// Widgets area above page content
					your_dress_create_widgets_area('widgets_above_page');
					?>				

					<div class="content">
						<?php
						// Widgets area inside page content
						your_dress_create_widgets_area('widgets_above_content');
						?>				
