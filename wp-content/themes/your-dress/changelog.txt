Version 1.4.0 
    Added: 
            - Compatibility with Gutenberg and other PageBuilders 
            - WP GDPR Compliance plugin 
            - Privacy Policy page 
            - Alert to Demo data installation  

    Fixed: 
            - Compatibility with WP 4.9.8 
            - Update plugins to their latest versions 
            - Improve theme styles 
            - Button shortcode (button aligment and "open in new window" option) 
            - Importer 
            - Update documentation 
            - WooCommerce 

    Changed: 
            - Update default contact forms to make them GDPR-compliant 
            - Move Importer to TRX Addons plugin 
            - Update demo data

Version 1.3.0
    Added:
        - Compatibility with YITH WooCommerce Wishlist
        - Compatibility with Woocommerce Easy Booking
        - Automatic update of current year in footer copyright area by using {Y} or {{Y}} symbols
    Fixed:
        - Compatibility with WooCommerce 3.3+
        - Compatibility with WordPress 4.9+
        - Compatibility with PHP 7
		- Styles for switching from Debug mode to normal mode
    Changed:
        - Move Importer to ThemeREX Addons plugin
        - "Rent a dress" page. It displays product categories now.
        - Icons for WooCommerce Products
		- Update documentation
    Removed:
        - Compatibility with Booked

Version 1.2
	Improve Shop layout page
	Improve sinlge product page layout
	Add zoom
	Update plugins

Version 1.1
	Update plugins
	Update documentation

Version 1.0
	Release