<?php
/**
 * The Sticky template to display the sticky posts
 *
 * Used for index/archive
 *
 * @package WordPress
 * @subpackage YOUR_DRESS
 * @since YOUR_DRESS 1.0
 */

$your_dress_columns = max(1, min(3, count(get_option( 'sticky_posts' ))));
$your_dress_post_format = get_post_format();
$your_dress_post_format = empty($your_dress_post_format) ? 'standard' : str_replace('post-format-', '', $your_dress_post_format);
$your_dress_animation = your_dress_get_theme_option('blog_animation');

?><div class="column-1_<?php echo esc_attr($your_dress_columns); ?>"><article id="post-<?php the_ID(); ?>" 
	<?php post_class( 'post_item post_layout_sticky post_format_'.esc_attr($your_dress_post_format) ); ?>
	<?php echo (!your_dress_is_off($your_dress_animation) ? ' data-animation="'.esc_attr(your_dress_get_animation_classes($your_dress_animation)).'"' : ''); ?>
	>

	<?php
	if ( is_sticky() && is_home() && !is_paged() ) {
		?><span class="post_label label_sticky"><?php
        echo esc_html__('Sticky','your-dress');
        ?></span><?php
	}

	// Featured image
	your_dress_show_post_featured(array(
		'thumb_size' => your_dress_get_thumb_size($your_dress_columns==1 ? 'big' : ($your_dress_columns==2 ? 'med' : 'avatar'))
	));

	if ( !in_array($your_dress_post_format, array('link', 'aside', 'status', 'quote')) ) {
		?>
		<div>
			<?php
            // Title and post meta
            if (get_the_title() != '') {
                ?>
                <div class="post_header entry-header">
                    <?php
                    do_action('your_dress_action_before_post_meta');
                    // Post meta
                    your_dress_show_post_meta(array(
                            'categories' => true,
                            'date' => false,
                            'author' => false,
                            'edit' => false,
                            'seo' => false,
                            'share' => false,
                            'counters' => 'comments'	//comments,likes,views - comma separated in any combination
                        )
                    );
                    do_action('your_dress_action_before_post_title');

                    if(!has_post_thumbnail()) {
                        $dt = apply_filters('your_dress_filter_get_post_date', your_dress_get_date('', 'j M'));
                        $date = explode(' ', $dt);
                        if (count($date) > 1) {
                            echo '<div class="post-date">';
                            echo '<span class="post-day">' . esc_html($date[0]) .'</span>';
                            echo '<span class="post-month">' . esc_html($date[1]) .'</span>';
                            echo '</div>';
                        }
                    }
                    // Post title
                    the_title( sprintf( '<h3 class="post_title entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h3>' );
                    ?>
                </div><!-- .post_header --><?php
                // Post content area
                ?><div class="post_content entry-content" itemprop="articleBody"><div class="post_content_inner"><?php
                if (has_excerpt()) {
                    the_excerpt();
                } else if (strpos(get_the_content('!--more'), '!--more')!==false) {
                    the_content( '' );
                } else if (in_array($your_dress_post_format, array('link', 'aside', 'status', 'quote'))) {
                    the_content();
                } else if (substr(get_the_content(), 0, 1)!='[') {
                    the_excerpt();
                }
                ?></div><?php
                // More button
                    ?><p><a class="more-link" href="<?php echo esc_url(get_permalink()); ?>"><?php esc_html_e('Read more', 'your-dress'); ?></a></p><?php
            }
			?>
            </div></div><!-- .entry-header -->
		<?php
	}
	?>
</article></div>