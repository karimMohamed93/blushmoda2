<?php
/**
 * The Gallery template to display posts
 *
 * Used for index/archive/search.
 *
 * @package WordPress
 * @subpackage YOUR_DRESS
 * @since YOUR_DRESS 1.0
 */

$your_dress_blog_style = explode('_', your_dress_get_theme_option('blog_style'));
$your_dress_columns = empty($your_dress_blog_style[1]) ? 2 : max(2, $your_dress_blog_style[1]);
$your_dress_post_format = get_post_format();
$your_dress_post_format = empty($your_dress_post_format) ? 'standard' : str_replace('post-format-', '', $your_dress_post_format);
$your_dress_animation = your_dress_get_theme_option('blog_animation');
$your_dress_image = wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()), 'full' );

?><article id="post-<?php the_ID(); ?>" 
	<?php post_class( 'post_item post_layout_portfolio post_layout_gallery post_layout_gallery_'.esc_attr($your_dress_columns).' post_format_'.esc_attr($your_dress_post_format) ); ?>
	<?php echo (!your_dress_is_off($your_dress_animation) ? ' data-animation="'.esc_attr(your_dress_get_animation_classes($your_dress_animation)).'"' : ''); ?>
	data-size="<?php if (!empty($your_dress_image[1]) && !empty($your_dress_image[2])) echo intval($your_dress_image[1]) .'x' . intval($your_dress_image[2]); ?>"
	data-src="<?php if (!empty($your_dress_image[0])) echo esc_url($your_dress_image[0]); ?>"
	>

	<?php
	$your_dress_image_hover = 'icon';	//your_dress_get_theme_option('image_hover');
	if (in_array($your_dress_image_hover, array('icons', 'zoom'))) $your_dress_image_hover = 'dots';
	// Featured image
	your_dress_show_post_featured(array(
		'hover' => $your_dress_image_hover,
		'thumb_size' => your_dress_get_thumb_size( strpos(your_dress_get_theme_option('body_style'), 'full')!==false || $your_dress_columns < 3 ? 'masonry-big' : 'masonry' ),
		'thumb_only' => true,
		'show_no_image' => true,
		'post_info' => '<div class="post_details">'
							. '<h2 class="post_title"><a href="'.esc_url(get_permalink()).'">'. esc_html(get_the_title()) . '</a></h2>'
							. '<div class="post_description">'
								. your_dress_show_post_meta(array(
									'categories' => true,
									'date' => true,
									'edit' => false,
									'seo' => false,
									'share' => true,
									'counters' => 'comments',
									'echo' => false
									))
								. '<div class="post_description_content">'
									. apply_filters('the_excerpt', get_the_excerpt())
								. '</div>'
								. '<a href="'.esc_url(get_permalink()).'" class="theme_button post_readmore"><span class="post_readmore_label">' . esc_html__('Learn more', 'your-dress') . '</span></a>'
							. '</div>'
						. '</div>'
	));
	?>
</article>