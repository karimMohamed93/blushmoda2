/* global jQuery:false */
/* global YOUR_DRESS_STORAGE:false */

jQuery(document).ready(function() {
	"use strict";

	// Init Media manager variables
	YOUR_DRESS_STORAGE['media_id'] = '';
	YOUR_DRESS_STORAGE['media_frame'] = [];
	YOUR_DRESS_STORAGE['media_link'] = [];
	jQuery('.your_dress_media_selector').on('click', function(e) {
		your_dress_show_media_manager(this);
		e.preventDefault();
		return false;
	});
	jQuery('.your_dress_meta_box_field_preview').on('click', '> span', function(e) {
		var image = jQuery(this);
		var button = image.parent().prev('.your_dress_media_selector');
		var field = jQuery('#'+button.data('linked-field'));
		if (field.length == 0) return;
		if (button.data('multiple')==1) {
			var val = field.val().split('|');
			val.splice(image.index(), 1);
			field.val(val.join('|'));
			image.remove();
		} else {
			field.val('');
			image.remove();
		}
		e.preventDefault();
		return false;
	});
	

	// Hide empty meta-boxes
	jQuery('.postbox > .inside').each(function() {
		if (jQuery(this).html().length < 5) jQuery(this).parent().hide();
	});

	// Hide admin notice
	jQuery('#your_dress_admin_notice .your_dress_hide_notice').on('click', function(e) {
		jQuery('#your_dress_admin_notice').slideUp();
		jQuery.post( YOUR_DRESS_STORAGE['ajax_url'], {'action': 'your_dress_hide_admin_notice'}, function(response){});
		e.preventDefault();
		return false;
	});
	
	// TGMPA Source selector is changed
	jQuery('.tgmpa_source_file').on('change', function(e) {
		var chk = jQuery(this).parents('tr').find('>th>input[type="checkbox"]');
		if (chk.length == 1) {
			if (jQuery(this).val() != '')
				chk.attr('checked', 'checked');
			else
				chk.removeAttr('checked');
		}
	});
		
	// Add icon selector after the menu item classes field
	jQuery('.edit-menu-item-classes').each(function() {
		var icon = your_dress_get_icon_class(jQuery(this).val());
		jQuery(this).after('<span class="your_dress_list_icons_selector'+(icon ? ' '+icon : '')+'" title="'+YOUR_DRESS_STORAGE['icon_selector_msg']+'"></span>');
	});
	jQuery('.your_dress_list_icons_selector').on('click', function(e) {
		var selector = jQuery(this);
		var input_id = selector.prev().attr('id');
		if (input_id === undefined) {
			input_id = ('your_dress_icon_field_'+Math.random()).replace(/\./g, '');
			selector.prev().attr('id', input_id)
		}
		var in_menu = selector.parents('.menu-item-settings').length > 0;
		var list = in_menu ? jQuery('.your_dress_list_icons') : selector.next('.your_dress_list_icons');
		if (list.length > 0) {
			list.find('span.your_dress_list_active').removeClass('your_dress_list_active');
			var icon = your_dress_get_icon_class(selector.attr('class'));
			if (icon != '') list.find('span[class*="'+icon+'"]').addClass('your_dress_list_active');
			var pos = in_menu ? selector.offset() : selector.position();
			list.data('input_id', input_id).css({'left': pos.left-(in_menu ? 0 : list.outerWidth()-selector.width()-1), 'top': pos.top+(in_menu ? 0 : selector.height()+4)}).fadeIn();
		}
		e.preventDefault();
		return false;
	});
	jQuery('.your_dress_list_icons span').on('click', function(e) {
		var list = jQuery(this).parent().fadeOut();
		var icon = your_dress_alltrim(jQuery(this).attr('class').replace(/your_dress_list_active/, ''));
		var input = jQuery('#'+list.data('input_id'));
		input.val(your_dress_chg_icon_class(input.val(), icon)).trigger('change');
		var selector = input.next();
		selector.attr('class', your_dress_chg_icon_class(selector.attr('class'), icon));
		e.preventDefault();
		return false;
	});

	// Standard WP Color Picker
	if (jQuery('.your_dress_color_selector').length > 0) {
		jQuery('.your_dress_color_selector').wpColorPicker({
			// you can declare a default color here,
			// or in the data-default-color attribute on the input
			//defaultColor: false,
	
			// a callback to fire whenever the color changes to a valid color
			change: function(e, ui){
				jQuery(e.target).val(ui.color).trigger('change');
			},
	
			// a callback to fire when the input is emptied or an invalid color
			clear: function(e) {
				jQuery(e.target).prev().trigger('change')
			},
	
			// hide the color picker controls on load
			//hide: true,
	
			// show a group of common colors beneath the square
			// or, supply an array of colors to customize further
			//palettes: true
		});
	}

	function your_dress_chg_icon_class(classes, icon) {
		var chg = false;
		classes = your_dress_alltrim(classes).split(' ');
		for (var i=0; i<classes.length; i++) {
			if (classes[i].indexOf('icon-') >= 0) {
				classes[i] = icon;
				chg = true;
				break;
			}
		}
		if (!chg) {
			if (classes.length == 1 && classes[0] == '')
				classes[0] = icon;
			else
				classes.push(icon);
		}
		return classes.join(' ');
	}

	function your_dress_get_icon_class(classes) {
		var classes = your_dress_alltrim(classes).split(' ');
		var icon = '';
		for (var i=0; i<classes.length; i++) {
			if (classes[i].indexOf('icon-') >= 0) {
				icon = classes[i];
				break;
			}
		}
		return icon;
	}

	function your_dress_show_media_manager(el) {
		YOUR_DRESS_STORAGE['media_id'] = jQuery(el).attr('id');
		YOUR_DRESS_STORAGE['media_link'][YOUR_DRESS_STORAGE['media_id']] = jQuery(el);
		// If the media frame already exists, reopen it.
		if ( YOUR_DRESS_STORAGE['media_frame'][YOUR_DRESS_STORAGE['media_id']] ) {
			YOUR_DRESS_STORAGE['media_frame'][YOUR_DRESS_STORAGE['media_id']].open();
			return false;
		}
		var type = YOUR_DRESS_STORAGE['media_link'][YOUR_DRESS_STORAGE['media_id']].data('type') 
						? YOUR_DRESS_STORAGE['media_link'][YOUR_DRESS_STORAGE['media_id']].data('type') 
						: 'image';
		var args = {
			// Set the title of the modal.
			title: YOUR_DRESS_STORAGE['media_link'][YOUR_DRESS_STORAGE['media_id']].data('choose'),
			// Multiple choise
			multiple: YOUR_DRESS_STORAGE['media_link'][YOUR_DRESS_STORAGE['media_id']].data('multiple')==1 
						? 'add' 
						: false,
			// Customize the submit button.
			button: {
				// Set the text of the button.
				text: YOUR_DRESS_STORAGE['media_link'][YOUR_DRESS_STORAGE['media_id']].data('update'),
				// Tell the button not to close the modal, since we're
				// going to refresh the page when the image is selected.
				close: true
			}
		};
		// Allow sizes and filters for the images
		if (type == 'image') {
			args['frame'] = 'post';
		}
		// Tell the modal to show only selected post types
		if (type == 'image' || type == 'audio' || type == 'video') {
			args['library'] = {
				type: type
			};
		}
		YOUR_DRESS_STORAGE['media_frame'][YOUR_DRESS_STORAGE['media_id']] = wp.media(args);

		// When an image is selected, run a callback.
		YOUR_DRESS_STORAGE['media_frame'][YOUR_DRESS_STORAGE['media_id']].on( 'insert select', function(selection) {
			// Grab the selected attachment.
			var field = jQuery("#"+YOUR_DRESS_STORAGE['media_link'][YOUR_DRESS_STORAGE['media_id']].data('linked-field')).eq(0);
			var attachment = null, attachment_url = '';
			if (YOUR_DRESS_STORAGE['media_link'][YOUR_DRESS_STORAGE['media_id']].data('multiple')===1) {
				YOUR_DRESS_STORAGE['media_frame'][YOUR_DRESS_STORAGE['media_id']].state().get('selection').map( function( att ) {
					attachment_url += (attachment_url ? "|" : "") + att.toJSON().url;
				});
				var val = field.val();
				attachment_url = val + (val ? "|" : '') + attachment_url;
			} else {
				attachment = YOUR_DRESS_STORAGE['media_frame'][YOUR_DRESS_STORAGE['media_id']].state().get('selection').first().toJSON();
				attachment_url = attachment.url;
				var sizes_selector = jQuery('.media-modal-content .attachment-display-settings select.size');
				if (sizes_selector.length > 0) {
					var size = your_dress_get_listbox_selected_value(sizes_selector.get(0));
					if (size != '') attachment_url = attachment.sizes[size].url;
				}
			}
			// Display images in the preview area
			var preview = field.siblings('.your_dress_meta_box_field_preview');
			if (preview.length == 0) {
				jQuery('<span class="your_dress_meta_box_field_preview"></span>').insertAfter(field);
				preview = field.siblings('.your_dress_meta_box_field_preview');
			}
			if (preview.length != 0) preview.empty();
			var images = attachment_url.split("|");
			for (var i=0; i<images.length; i++) {
				if (preview.length != 0) {
					var ext = your_dress_get_file_ext(images[i]);
					preview.append('<span>'
									+ (ext=='gif' || ext=='jpg' || ext=='jpeg' || ext=='png' 
											? '<img src="'+images[i]+'">'
											: '<a href="'+images[i]+'">'+your_dress_get_file_name(images[i])+'</a>'
										)
									+ '</span>');
				}
			}
			// Update field
			field.val(attachment_url).trigger('change');
		});

		// Finally, open the modal.
		YOUR_DRESS_STORAGE['media_frame'][YOUR_DRESS_STORAGE['media_id']].open();
		return false;
	}

});