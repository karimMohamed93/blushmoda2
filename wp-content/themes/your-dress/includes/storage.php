<?php
/**
 * Theme storage manipulations
 *
 * @package WordPress
 * @subpackage YOUR_DRESS
 * @since YOUR_DRESS 1.0
 */

// Disable direct call
if ( ! defined( 'ABSPATH' ) ) { exit; }

// Get theme variable
if (!function_exists('your_dress_storage_get')) {
	function your_dress_storage_get($var_name, $default='') {
		global $YOUR_DRESS_STORAGE;
		return isset($YOUR_DRESS_STORAGE[$var_name]) ? $YOUR_DRESS_STORAGE[$var_name] : $default;
	}
}

// Set theme variable
if (!function_exists('your_dress_storage_set')) {
	function your_dress_storage_set($var_name, $value) {
		global $YOUR_DRESS_STORAGE;
		$YOUR_DRESS_STORAGE[$var_name] = $value;
	}
}

// Check if theme variable is empty
if (!function_exists('your_dress_storage_empty')) {
	function your_dress_storage_empty($var_name, $key='', $key2='') {
		global $YOUR_DRESS_STORAGE;
		if (!empty($key) && !empty($key2))
			return empty($YOUR_DRESS_STORAGE[$var_name][$key][$key2]);
		else if (!empty($key))
			return empty($YOUR_DRESS_STORAGE[$var_name][$key]);
		else
			return empty($YOUR_DRESS_STORAGE[$var_name]);
	}
}

// Check if theme variable is set
if (!function_exists('your_dress_storage_isset')) {
	function your_dress_storage_isset($var_name, $key='', $key2='') {
		global $YOUR_DRESS_STORAGE;
		if (!empty($key) && !empty($key2))
			return isset($YOUR_DRESS_STORAGE[$var_name][$key][$key2]);
		else if (!empty($key))
			return isset($YOUR_DRESS_STORAGE[$var_name][$key]);
		else
			return isset($YOUR_DRESS_STORAGE[$var_name]);
	}
}

// Inc/Dec theme variable with specified value
if (!function_exists('your_dress_storage_inc')) {
	function your_dress_storage_inc($var_name, $value=1) {
		global $YOUR_DRESS_STORAGE;
		if (empty($YOUR_DRESS_STORAGE[$var_name])) $YOUR_DRESS_STORAGE[$var_name] = 0;
		$YOUR_DRESS_STORAGE[$var_name] += $value;
	}
}

// Concatenate theme variable with specified value
if (!function_exists('your_dress_storage_concat')) {
	function your_dress_storage_concat($var_name, $value) {
		global $YOUR_DRESS_STORAGE;
		if (empty($YOUR_DRESS_STORAGE[$var_name])) $YOUR_DRESS_STORAGE[$var_name] = '';
		$YOUR_DRESS_STORAGE[$var_name] .= $value;
	}
}

// Get array (one or two dim) element
if (!function_exists('your_dress_storage_get_array')) {
	function your_dress_storage_get_array($var_name, $key, $key2='', $default='') {
		global $YOUR_DRESS_STORAGE;
		if (empty($key2))
			return !empty($var_name) && !empty($key) && isset($YOUR_DRESS_STORAGE[$var_name][$key]) ? $YOUR_DRESS_STORAGE[$var_name][$key] : $default;
		else
			return !empty($var_name) && !empty($key) && isset($YOUR_DRESS_STORAGE[$var_name][$key][$key2]) ? $YOUR_DRESS_STORAGE[$var_name][$key][$key2] : $default;
	}
}

// Set array element
if (!function_exists('your_dress_storage_set_array')) {
	function your_dress_storage_set_array($var_name, $key, $value) {
		global $YOUR_DRESS_STORAGE;
		if (!isset($YOUR_DRESS_STORAGE[$var_name])) $YOUR_DRESS_STORAGE[$var_name] = array();
		if ($key==='')
			$YOUR_DRESS_STORAGE[$var_name][] = $value;
		else
			$YOUR_DRESS_STORAGE[$var_name][$key] = $value;
	}
}

// Set two-dim array element
if (!function_exists('your_dress_storage_set_array2')) {
	function your_dress_storage_set_array2($var_name, $key, $key2, $value) {
		global $YOUR_DRESS_STORAGE;
		if (!isset($YOUR_DRESS_STORAGE[$var_name])) $YOUR_DRESS_STORAGE[$var_name] = array();
		if (!isset($YOUR_DRESS_STORAGE[$var_name][$key])) $YOUR_DRESS_STORAGE[$var_name][$key] = array();
		if ($key2==='')
			$YOUR_DRESS_STORAGE[$var_name][$key][] = $value;
		else
			$YOUR_DRESS_STORAGE[$var_name][$key][$key2] = $value;
	}
}

// Merge array elements
if (!function_exists('your_dress_storage_merge_array')) {
	function your_dress_storage_merge_array($var_name, $key, $value) {
		global $YOUR_DRESS_STORAGE;
		if (!isset($YOUR_DRESS_STORAGE[$var_name])) $YOUR_DRESS_STORAGE[$var_name] = array();
		if ($key==='')
			$YOUR_DRESS_STORAGE[$var_name] = array_merge($YOUR_DRESS_STORAGE[$var_name], $value);
		else
			$YOUR_DRESS_STORAGE[$var_name][$key] = array_merge($YOUR_DRESS_STORAGE[$var_name][$key], $value);
	}
}

// Add array element after the key
if (!function_exists('your_dress_storage_set_array_after')) {
	function your_dress_storage_set_array_after($var_name, $after, $key, $value='') {
		global $YOUR_DRESS_STORAGE;
		if (!isset($YOUR_DRESS_STORAGE[$var_name])) $YOUR_DRESS_STORAGE[$var_name] = array();
		if (is_array($key))
			your_dress_array_insert_after($YOUR_DRESS_STORAGE[$var_name], $after, $key);
		else
			your_dress_array_insert_after($YOUR_DRESS_STORAGE[$var_name], $after, array($key=>$value));
	}
}

// Add array element before the key
if (!function_exists('your_dress_storage_set_array_before')) {
	function your_dress_storage_set_array_before($var_name, $before, $key, $value='') {
		global $YOUR_DRESS_STORAGE;
		if (!isset($YOUR_DRESS_STORAGE[$var_name])) $YOUR_DRESS_STORAGE[$var_name] = array();
		if (is_array($key))
			your_dress_array_insert_before($YOUR_DRESS_STORAGE[$var_name], $before, $key);
		else
			your_dress_array_insert_before($YOUR_DRESS_STORAGE[$var_name], $before, array($key=>$value));
	}
}

// Push element into array
if (!function_exists('your_dress_storage_push_array')) {
	function your_dress_storage_push_array($var_name, $key, $value) {
		global $YOUR_DRESS_STORAGE;
		if (!isset($YOUR_DRESS_STORAGE[$var_name])) $YOUR_DRESS_STORAGE[$var_name] = array();
		if ($key==='')
			array_push($YOUR_DRESS_STORAGE[$var_name], $value);
		else {
			if (!isset($YOUR_DRESS_STORAGE[$var_name][$key])) $YOUR_DRESS_STORAGE[$var_name][$key] = array();
			array_push($YOUR_DRESS_STORAGE[$var_name][$key], $value);
		}
	}
}

// Pop element from array
if (!function_exists('your_dress_storage_pop_array')) {
	function your_dress_storage_pop_array($var_name, $key='', $defa='') {
		global $YOUR_DRESS_STORAGE;
		$rez = $defa;
		if ($key==='') {
			if (isset($YOUR_DRESS_STORAGE[$var_name]) && is_array($YOUR_DRESS_STORAGE[$var_name]) && count($YOUR_DRESS_STORAGE[$var_name]) > 0) 
				$rez = array_pop($YOUR_DRESS_STORAGE[$var_name]);
		} else {
			if (isset($YOUR_DRESS_STORAGE[$var_name][$key]) && is_array($YOUR_DRESS_STORAGE[$var_name][$key]) && count($YOUR_DRESS_STORAGE[$var_name][$key]) > 0) 
				$rez = array_pop($YOUR_DRESS_STORAGE[$var_name][$key]);
		}
		return $rez;
	}
}

// Inc/Dec array element with specified value
if (!function_exists('your_dress_storage_inc_array')) {
	function your_dress_storage_inc_array($var_name, $key, $value=1) {
		global $YOUR_DRESS_STORAGE;
		if (!isset($YOUR_DRESS_STORAGE[$var_name])) $YOUR_DRESS_STORAGE[$var_name] = array();
		if (empty($YOUR_DRESS_STORAGE[$var_name][$key])) $YOUR_DRESS_STORAGE[$var_name][$key] = 0;
		$YOUR_DRESS_STORAGE[$var_name][$key] += $value;
	}
}

// Concatenate array element with specified value
if (!function_exists('your_dress_storage_concat_array')) {
	function your_dress_storage_concat_array($var_name, $key, $value) {
		global $YOUR_DRESS_STORAGE;
		if (!isset($YOUR_DRESS_STORAGE[$var_name])) $YOUR_DRESS_STORAGE[$var_name] = array();
		if (empty($YOUR_DRESS_STORAGE[$var_name][$key])) $YOUR_DRESS_STORAGE[$var_name][$key] = '';
		$YOUR_DRESS_STORAGE[$var_name][$key] .= $value;
	}
}

// Call object's method
if (!function_exists('your_dress_storage_call_obj_method')) {
	function your_dress_storage_call_obj_method($var_name, $method, $param=null) {
		global $YOUR_DRESS_STORAGE;
		if ($param===null)
			return !empty($var_name) && !empty($method) && isset($YOUR_DRESS_STORAGE[$var_name]) ? $YOUR_DRESS_STORAGE[$var_name]->$method(): '';
		else
			return !empty($var_name) && !empty($method) && isset($YOUR_DRESS_STORAGE[$var_name]) ? $YOUR_DRESS_STORAGE[$var_name]->$method($param): '';
	}
}

// Get object's property
if (!function_exists('your_dress_storage_get_obj_property')) {
	function your_dress_storage_get_obj_property($var_name, $prop, $default='') {
		global $YOUR_DRESS_STORAGE;
		return !empty($var_name) && !empty($prop) && isset($YOUR_DRESS_STORAGE[$var_name]->$prop) ? $YOUR_DRESS_STORAGE[$var_name]->$prop : $default;
	}
}
?>