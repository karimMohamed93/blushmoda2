<?php
/**
 * The template for homepage posts with "Portfolio" style
 *
 * @package WordPress
 * @subpackage YOUR_DRESS
 * @since YOUR_DRESS 1.0
 */

your_dress_storage_set('blog_archive', true);

// Load scripts for both 'Gallery' and 'Portfolio' layouts!
wp_enqueue_script( 'classie', your_dress_get_file_url('js/theme.gallery/classie.min.js'), array(), null, true );
wp_enqueue_script( 'imagesloaded', your_dress_get_file_url('js/theme.gallery/imagesloaded.min.js'), array(), null, true );
wp_enqueue_script( 'masonry', your_dress_get_file_url('js/theme.gallery/masonry.min.js'), array(), null, true );
wp_enqueue_script( 'your_dress-gallery-script', your_dress_get_file_url('js/theme.gallery/theme.gallery.js'), array(), null, true );

get_header(); 

if (have_posts()) {

	echo get_query_var('blog_archive_start');

	$your_dress_stickies = is_home() ? get_option( 'sticky_posts' ) : false;
	$your_dress_sticky_out = is_array($your_dress_stickies) && count($your_dress_stickies) > 0 && get_query_var( 'paged' ) < 1;
	
	// Show filters
	$your_dress_cat = your_dress_get_theme_option('parent_cat');
	$your_dress_post_type = your_dress_get_theme_option('post_type');
	$your_dress_taxonomy = your_dress_get_post_type_taxonomy($your_dress_post_type);
	$your_dress_show_filters = your_dress_get_theme_option('show_filters');
	$your_dress_tabs = array();
	if (!your_dress_is_off($your_dress_show_filters)) {
		$your_dress_args = array(
			'type'			=> $your_dress_post_type,
			'child_of'		=> $your_dress_cat,
			'orderby'		=> 'name',
			'order'			=> 'ASC',
			'hide_empty'	=> 1,
			'hierarchical'	=> 0,
			'exclude'		=> '',
			'include'		=> '',
			'number'		=> '',
			'taxonomy'		=> $your_dress_taxonomy,
			'pad_counts'	=> false
		);
		$your_dress_portfolio_list = get_terms($your_dress_args);
		if (is_array($your_dress_portfolio_list) && count($your_dress_portfolio_list) > 0) {
			$your_dress_tabs[$your_dress_cat] = esc_html__('All', 'your-dress');
			foreach ($your_dress_portfolio_list as $your_dress_term) {
				if (isset($your_dress_term->term_id)) $your_dress_tabs[$your_dress_term->term_id] = $your_dress_term->name;
			}
		}
	}
	if (count($your_dress_tabs) > 0) {
		$your_dress_portfolio_filters_ajax = true;
		$your_dress_portfolio_filters_active = $your_dress_cat;
		$your_dress_portfolio_filters_id = 'portfolio_filters';
		if (!is_customize_preview())
			wp_enqueue_script('jquery-ui-tabs', false, array('jquery', 'jquery-ui-core'), null, true);
		?>
		<div class="portfolio_filters your_dress_tabs your_dress_tabs_ajax">
			<ul class="portfolio_titles your_dress_tabs_titles">
				<?php
				foreach ($your_dress_tabs as $your_dress_id=>$your_dress_title) {
					?><li><a href="<?php echo esc_url(your_dress_get_hash_link(sprintf('#%s_%s_content', $your_dress_portfolio_filters_id, $your_dress_id))); ?>" data-tab="<?php echo esc_attr($your_dress_id); ?>"><?php echo esc_html($your_dress_title); ?></a></li><?php
				}
				?>
			</ul>
			<?php
			$your_dress_ppp = your_dress_get_theme_option('posts_per_page');
			if (your_dress_is_inherit($your_dress_ppp)) $your_dress_ppp = '';
			foreach ($your_dress_tabs as $your_dress_id=>$your_dress_title) {
				$your_dress_portfolio_need_content = $your_dress_id==$your_dress_portfolio_filters_active || !$your_dress_portfolio_filters_ajax;
				?>
				<div id="<?php echo esc_attr(sprintf('%s_%s_content', $your_dress_portfolio_filters_id, $your_dress_id)); ?>"
					class="portfolio_content your_dress_tabs_content"
					data-blog-template="<?php echo esc_attr(your_dress_storage_get('blog_template')); ?>"
					data-blog-style="<?php echo esc_attr(your_dress_get_theme_option('blog_style')); ?>"
					data-posts-per-page="<?php echo esc_attr($your_dress_ppp); ?>"
					data-post-type="<?php echo esc_attr($your_dress_post_type); ?>"
					data-taxonomy="<?php echo esc_attr($your_dress_taxonomy); ?>"
					data-cat="<?php echo esc_attr($your_dress_id); ?>"
					data-parent-cat="<?php echo esc_attr($your_dress_cat); ?>"
					data-need-content="<?php echo (false===$your_dress_portfolio_need_content ? 'true' : 'false'); ?>"
				>
					<?php
					if ($your_dress_portfolio_need_content) 
						your_dress_show_portfolio_posts(array(
							'cat' => $your_dress_id,
							'parent_cat' => $your_dress_cat,
							'taxonomy' => $your_dress_taxonomy,
							'post_type' => $your_dress_post_type,
							'page' => 1,
							'sticky' => $your_dress_sticky_out
							)
						);
					?>
				</div>
				<?php
			}
			?>
		</div>
		<?php
	} else {
		your_dress_show_portfolio_posts(array(
			'cat' => $your_dress_cat,
			'parent_cat' => $your_dress_cat,
			'taxonomy' => $your_dress_taxonomy,
			'post_type' => $your_dress_post_type,
			'page' => 1,
			'sticky' => $your_dress_sticky_out
			)
		);
	}

	echo get_query_var('blog_archive_end');

} else {

	if ( is_search() )
		get_template_part( 'content', 'none-search' );
	else
		get_template_part( 'content', 'none-archive' );

}

get_footer();
?>