<?php
/**
 * The Classic template to display the content
 *
 * Used for index/archive/search.
 *
 * @package WordPress
 * @subpackage YOUR_DRESS
 * @since YOUR_DRESS 1.0
 */

$your_dress_blog_style = explode('_', your_dress_get_theme_option('blog_style'));
$your_dress_columns = empty($your_dress_blog_style[1]) ? 2 : max(2, $your_dress_blog_style[1]);
$your_dress_expanded = !your_dress_sidebar_present() && your_dress_is_on(your_dress_get_theme_option('expand_content'));
$your_dress_post_format = get_post_format();
$your_dress_post_format = empty($your_dress_post_format) ? 'standard' : str_replace('post-format-', '', $your_dress_post_format);
$your_dress_animation = your_dress_get_theme_option('blog_animation');

?><div class="<?php echo trim($your_dress_blog_style[0] == 'classic' ? 'column' : 'masonry_item masonry_item'); ?>-1_<?php echo esc_attr($your_dress_columns); ?>"><article id="post-<?php the_ID(); ?>"
	<?php post_class( 'post_item post_format_'.esc_attr($your_dress_post_format)
					. ' post_layout_classic post_layout_classic_'.esc_attr($your_dress_columns)
					. ' post_layout_'.esc_attr($your_dress_blog_style[0]) 
					. ' post_layout_'.esc_attr($your_dress_blog_style[0]).'_'.esc_attr($your_dress_columns)
					); ?>
	<?php echo (!your_dress_is_off($your_dress_animation) ? ' data-animation="'.esc_attr(your_dress_get_animation_classes($your_dress_animation)).'"' : ''); ?>
	>

	<?php

	// Featured image
	your_dress_show_post_featured( array( 'thumb_size' => your_dress_get_thumb_size($your_dress_blog_style[0] == 'classic'
													? (strpos(your_dress_get_theme_option('body_style'), 'full')!==false 
															? ( $your_dress_columns > 2 ? 'big' : 'huge' )
															: (	$your_dress_columns > 2
																? ($your_dress_expanded ? 'med' : 'small')
																: ($your_dress_expanded ? 'big' : 'med')
																)
														)
													: (strpos(your_dress_get_theme_option('body_style'), 'full')!==false 
															? ( $your_dress_columns > 2 ? 'masonry-big' : 'full' )
															: (	$your_dress_columns <= 2 && $your_dress_expanded ? 'masonry-big' : 'masonry')
														)
								) ) );

	if ( !in_array($your_dress_post_format, array('link', 'aside', 'status', 'quote')) ) {
		?>
		<div class="post_header entry-header">
			<?php 
			do_action('your_dress_action_before_post_title'); 

			// Post title
			the_title( sprintf( '<h4 class="post_title entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h4>' );

			do_action('your_dress_action_before_post_meta'); 

			// Post meta
            your_dress_show_post_meta(array(
                    'categories' => true,
                    'date' => false,
                    'author' => true,
                    'edit' => false,
                    'seo' => false,
                    'share' => false,
                    'counters' => ''	//comments,likes,views - comma separated in any combination
                )
            );
			?>
		</div><!-- .entry-header -->
		<?php
	}		
	?>

	<div class="post_content entry-content">
		<div class="post_content_inner">
			<?php
			$your_dress_show_learn_more = false; //!in_array($your_dress_post_format, array('link', 'aside', 'status', 'quote'));
			if (has_excerpt()) {
				the_excerpt();
			} else if (strpos(get_the_content('!--more'), '!--more')!==false) {
				the_content( '' );
			} else if (in_array($your_dress_post_format, array('link', 'aside', 'status', 'quote'))) {
				the_content();
			} else if (substr(get_the_content(), 0, 1)!='[') {
				the_excerpt();
			}
			?>
		</div>
		<?php
		// Post meta
		if (in_array($your_dress_post_format, array('link', 'aside', 'status', 'quote'))) {
			your_dress_show_post_meta(array(
				'share' => false,
				'counters' => 'comments'
				)
			);
		}
		// More button
		if ( $your_dress_show_learn_more ) {
			?><p><a class="more-link" href="<?php echo esc_url(get_permalink()); ?>"><?php esc_html_e('Read more', 'your-dress'); ?></a></p><?php
		}
		?>
	</div><!-- .entry-content -->

</article></div>