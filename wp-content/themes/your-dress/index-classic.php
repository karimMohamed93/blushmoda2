<?php
/**
 * The template for homepage posts with "Classic" style
 *
 * @package WordPress
 * @subpackage YOUR_DRESS
 * @since YOUR_DRESS 1.0
 */

your_dress_storage_set('blog_archive', true);

// Load scripts for 'Masonry' layout
if (substr(your_dress_get_theme_option('blog_style'), 0, 7) == 'masonry') {
	wp_enqueue_script( 'classie', your_dress_get_file_url('js/theme.gallery/classie.min.js'), array(), null, true );
	wp_enqueue_script( 'imagesloaded', your_dress_get_file_url('js/theme.gallery/imagesloaded.min.js'), array(), null, true );
	wp_enqueue_script( 'masonry', your_dress_get_file_url('js/theme.gallery/masonry.min.js'), array(), null, true );
	wp_enqueue_script( 'your_dress-gallery-script', your_dress_get_file_url('js/theme.gallery/theme.gallery.js'), array(), null, true );
}

get_header(); 

if (have_posts()) {

	echo get_query_var('blog_archive_start');

	$your_dress_classes = 'posts_container '
						. (substr(your_dress_get_theme_option('blog_style'), 0, 7) == 'classic' ? 'columns_wrap' : 'masonry_wrap');
	$your_dress_stickies = is_home() ? get_option( 'sticky_posts' ) : false;
	$your_dress_sticky_out = is_array($your_dress_stickies) && count($your_dress_stickies) > 0 && get_query_var( 'paged' ) < 1;
	if ($your_dress_sticky_out) {
		?><div class="sticky_wrap columns_wrap"><?php	
	}
	if (!$your_dress_sticky_out) {
		if (your_dress_get_theme_option('first_post_large') && !is_paged() && !in_array(your_dress_get_theme_option('body_style'), array('fullwide', 'fullscreen'))) {
			the_post();
			get_template_part( 'content', 'excerpt' );
		}
		
		?><div class="<?php echo esc_attr($your_dress_classes); ?>"><?php
	}
	while ( have_posts() ) { the_post(); 
		if ($your_dress_sticky_out && !is_sticky()) {
			$your_dress_sticky_out = false;
			?></div><div class="<?php echo esc_attr($your_dress_classes); ?>"><?php
		}
		get_template_part( 'content', $your_dress_sticky_out && is_sticky() ? 'sticky' : 'classic' );
	}
	
	?></div><?php

	your_dress_show_pagination();

	echo get_query_var('blog_archive_end');

} else {

	if ( is_search() )
		get_template_part( 'content', 'none-search' );
	else
		get_template_part( 'content', 'none-archive' );

}

get_footer();
?>