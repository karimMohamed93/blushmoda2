<?php
//Custom Layouts
$layouts = array(
		'custom_639' => array(
				'name' => 'Ask a stylist',
				'template' => '[vc_row full_width=\"stretch_row_content_no_spaces\" css=\".vc_custom_1492693565458{background-image: url(http://your-dress.ancorathemes.com/wp-content/uploads/2017/03/background-5.jpg?id=333) !important;background-position: center !important;background-repeat: no-repeat !important;background-size: cover !important;}\"][vc_column width=\"7/12\" icons_position=\"left\"][/vc_column][vc_column width=\"1/3\" icons_position=\"left\" offset=\"vc_col-sm-offset-0 vc_col-xs-offset-1 vc_col-xs-10\"][vc_empty_space alter_height=\"huge\" hide_on_mobile=\"\"][vc_empty_space alter_height=\"huge\" hide_on_mobile=\"\" css=\".vc_custom_1492694076543{margin-top: -1rem !important;}\"][trx_sc_title title_style=\"default\" title_tag=\"h1\" title=\"Great Look With Our Stylist\" subtitle=\"ask a stylist\" description=\"If you don’t know what to wear and need some styling advice you have an opportunity to book an appointment with one of our stylists.\"][vc_empty_space height=\"1.55rem\" alter_height=\"none\" hide_on_mobile=\"\"][vc_column_text][contact-form-7 id=\"4\" title=\"Contact form 1\"][/vc_column_text][vc_empty_space alter_height=\"huge\" hide_on_mobile=\"\"][vc_empty_space alter_height=\"huge\" hide_on_mobile=\"\" css=\".vc_custom_1492694306503{margin-top: -0.6rem !important;}\"][/vc_column][/vc_row]',
				'meta' => array(
						'trx_addons_options' => array(
								'layout_type' => 'custom'
								),
						'_wpb_shortcodes_custom_css' => '.vc_custom_1492693565458{background-image: url(http://your-dress.ancorathemes.com/wp-content/uploads/2017/03/background-5.jpg?id=333) !important;background-position: center !important;background-repeat: no-repeat !important;background-size: cover !important;}.vc_custom_1492694076543{margin-top: -1rem !important;}.vc_custom_1492694306503{margin-top: -0.6rem !important;}'
						)
				),
		'custom_517' => array(
				'name' => 'Beautiful accessories',
				'template' => '[vc_row full_width=\"stretch_row_content_no_spaces\" scheme=\"dark\" css=\".vc_custom_1492581680073{background-image: url(http://your-dress.ancorathemes.com/wp-content/uploads/2017/03/bg2.jpg?id=322) !important;background-position: center !important;background-repeat: no-repeat !important;background-size: cover !important;}\"][vc_column width=\"5/12\" icons_position=\"left\" offset=\"vc_col-lg-offset-1 vc_col-md-offset-1 vc_col-sm-offset-1 vc_col-xs-offset-1 vc_col-xs-10\"][vc_empty_space alter_height=\"huge\" hide_on_mobile=\"\" css=\".vc_custom_1492582101730{margin-top: 1.35rem !important;}\"][trx_sc_title title_style=\"default\" title_tag=\"h1\" title=\"Complete Your {{Perfect Look}} \" subtitle=\"beautiful accessories\" description=\"Complete your look with fascinators, clutches and jewellery. Look absolutely glamorous and perfect at your event day!\"][vc_empty_space height=\"1.8rem\" alter_height=\"none\" hide_on_mobile=\"\"][product_category per_page=\"4\" columns=\"4\" orderby=\"date\" order=\"DESC\" category=\"gala\"][vc_empty_space alter_height=\"large\" hide_on_mobile=\"\" css=\".vc_custom_1492583211207{margin-top: 0.5rem !important;}\"][/vc_column][/vc_row]',
				'meta' => array(
						'trx_addons_options' => array(
								'layout_type' => 'custom'
								),
						'_wpb_shortcodes_custom_css' => '.vc_custom_1492581680073{background-image: url(http://your-dress.ancorathemes.com/wp-content/uploads/2017/03/bg2.jpg?id=322) !important;background-position: center !important;background-repeat: no-repeat !important;background-size: cover !important;}.vc_custom_1492582101730{margin-top: 1.35rem !important;}.vc_custom_1492583211207{margin-top: 0.5rem !important;}'
						)
				),
		'custom_482' => array(
				'name' => 'Contacts',
				'template' => '[vc_row full_width=\"stretch_row_content_no_spaces\" equal_height=\"yes\" content_placement=\"middle\"][vc_column icons_position=\"left\" css=\".vc_custom_1493114857000{background-color: #f6f4f0 !important;}\" offset=\"vc_col-lg-6 vc_col-md-8\"][vc_row_inner][vc_column_inner width=\"1/6\" icons_position=\"left\"][/vc_column_inner][vc_column_inner width=\"2/3\" icons_position=\"left\" offset=\"vc_col-sm-offset-0 vc_col-xs-offset-1 vc_col-xs-10\"][vc_empty_space alter_height=\"large\" hide_on_mobile=\"\"][trx_sc_title title_style=\"default\" title_tag=\"h2\" title=\"Get In Touch {{With Us}} \" subtitle=\"Our contacts\"][vc_empty_space alter_height=\"medium\" hide_on_mobile=\"\"][trx_sc_icons align=\"left\" size=\"small\" icons_animation=\"\" icons=\"%5B%7B%22title%22%3A%22%40FindYourDress%22%2C%22link%22%3A%22%23%22%2C%22description%22%3A%22Follow%20us%20on%20instagram%22%2C%22icon%22%3A%22icon-insta%22%7D%2C%7B%22title%22%3A%22123%20New%20York%2C%20NY%2060606%22%2C%22description%22%3A%22Try%20on%20our%20dresses%20in%20showroom%22%2C%22icon%22%3A%22icon-002-placeholder%22%7D%2C%7B%22title%22%3A%220%20800%20555%2044%2011%22%2C%22description%22%3A%22Contact%20us%20by%20telephone%22%2C%22icon%22%3A%22icon-001-technology%22%7D%2C%7B%22title%22%3A%22yourdress%40info.com%22%2C%22link%22%3A%22%23%22%2C%22description%22%3A%22Write%20us%20a%20message%22%2C%22icon%22%3A%22icon-003-envelope%22%7D%5D\" title_style=\"default\" columns=\"2\"][vc_empty_space alter_height=\"small\" hide_on_mobile=\"\"][/vc_column_inner][/vc_row_inner][/vc_column][vc_column icons_position=\"left\" offset=\"vc_col-lg-6 vc_col-md-4\"][trx_sc_googlemap style=\"grey\" zoom=\"12\" height=\"647\" markers=\"%5B%7B%22address%22%3A%22350%205th%20Ave%2C%20New%20York%2C%20NY%2010118%2C%20USA%22%2C%22icon%22%3A%22310%22%7D%5D\" title_style=\"default\"][/trx_sc_googlemap][/vc_column][/vc_row]',
				'meta' => array(
						'trx_addons_options' => array(
								'layout_type' => 'custom'
								),
						'_wpb_shortcodes_custom_css' => '.vc_custom_1493114857000{background-color: #f6f4f0 !important;}'
						)
				),
		'custom_653' => array(
				'name' => 'Customer reviews',
				'template' => '[vc_row full_width=\"stretch_row_content_no_spaces\" css=\".vc_custom_1492597011982{background-image: url(http://your-dress.ancorathemes.com/wp-content/uploads/2017/03/bg3.jpg?id=308) !important;background-position: center !important;background-repeat: no-repeat !important;background-size: cover !important;}\"][vc_column width=\"5/12\" icons_position=\"left\" offset=\"vc_col-lg-6 vc_col-md-6\"][/vc_column][vc_column width=\"1/2\" icons_position=\"left\" css=\".vc_custom_1493184356144{padding-left: 5.4rem !important;}\" offset=\"vc_col-lg-5 vc_col-md-5 vc_col-sm-offset-0 vc_col-xs-offset-1 vc_col-xs-10\"][vc_empty_space alter_height=\"huge\" hide_on_mobile=\"\" css=\".vc_custom_1492597406277{margin-bottom: -0.75rem !important;}\"][trx_sc_testimonials type=\"default\" orderby=\"post_date\" order=\"desc\" slider=\"1\" slider_pagination=\"right\" slider_pagination_thumbs=\"\" title_style=\"default\" title_tag=\"h1\" title_align=\"left\" count=\"3\" columns=\"1\" title=\"What Our Clients {{Are Saying}} \" subtitle=\"Customer reviews\"][vc_empty_space alter_height=\"small\" hide_on_mobile=\"\"][vc_row_inner][vc_column_inner icons_position=\"left\" offset=\"vc_hidden-md vc_hidden-sm vc_hidden-xs\"][ess_grid alias=\"Home\"][vc_empty_space alter_height=\"small\" hide_on_mobile=\"\"][/vc_column_inner][/vc_row_inner][trx_sc_button type=\"simple\" align=\"right\" icon_position=\"left\" title=\"all photos\" link=\"/grid/\"][vc_empty_space alter_height=\"huge\" hide_on_mobile=\"\" css=\".vc_custom_1492611249360{margin-bottom: -0.55rem !important;}\"][/vc_column][/vc_row]',
				'meta' => array(
						'trx_addons_options' => array(
								'layout_type' => 'custom'
								),
						'_wpb_shortcodes_custom_css' => '.vc_custom_1492597011982{background-image: url(http://your-dress.ancorathemes.com/wp-content/uploads/2017/03/bg3.jpg?id=308) !important;background-position: center !important;background-repeat: no-repeat !important;background-size: cover !important;}.vc_custom_1493184356144{padding-left: 5.4rem !important;}.vc_custom_1492597406277{margin-bottom: -0.75rem !important;}.vc_custom_1492611249360{margin-bottom: -0.55rem !important;}'
						)
				),
		'footer_10' => array(
				'name' => 'Footer Informed',
				'template' => '[vc_row full_width=\"stretch_row\" row_type=\"normal\" row_delimiter=\"\" row_fixed=\"\" hide_on_mobile=\"\" hide_on_frontpage=\"\" css=\".vc_custom_1489074210606{background-color: #413e49 !important;}\"][vc_column column_align=\"left\" icons_position=\"left\"][trx_sc_content size=\"1_1\" number_position=\"br\" title_style=\"default\"][vc_row_inner][vc_column_inner width=\"1/4\" icons_position=\"left\"][vc_wp_custommenu nav_menu=\"27\" title=\"Hire Dresses\"][vc_empty_space height=\"\" hide_on_mobile=\"\"][/vc_column_inner][vc_column_inner width=\"1/4\" icons_position=\"left\"][vc_wp_custommenu nav_menu=\"28\" title=\"Customer Care\"][vc_empty_space height=\"\" hide_on_mobile=\"\"][/vc_column_inner][vc_column_inner width=\"1/4\" icons_position=\"left\"][vc_wp_custommenu nav_menu=\"29\" title=\"About Us\"][vc_empty_space height=\"\" hide_on_mobile=\"\"][/vc_column_inner][vc_column_inner width=\"1/4\" icons_position=\"left\"][trx_widget_contacts columns=\"\" googlemap=\"\" socials=\"1\" title=\"Stay Connected\"][/trx_widget_contacts][vc_empty_space height=\"\" hide_on_mobile=\"\"][/vc_column_inner][/vc_row_inner][/trx_sc_content][/vc_column][/vc_row][vc_row full_width=\"stretch_row\" scheme=\"dark\" row_type=\"compact\" row_delimiter=\"\" row_fixed=\"\" hide_on_tablet=\"\" hide_on_mobile=\"\" hide_on_frontpage=\"\" css=\".vc_custom_1491807515465{background-color: #393741 !important;}\"][vc_column column_align=\"left\" icons_position=\"left\"][trx_sc_content size=\"1_1\" float=\"center\" number_position=\"br\" title_style=\"default\"][vc_wp_text]<a href=\"https://themeforest.net/user/ancorathemes/portfolio\" target=\"_blank\" rel=\"noopener\">AncoraThemes</a> &copy; {Y}. All Rights Reserved. <a href=\"http://ancorathemes.com/about/\" target=\"_blank\" rel=\"noopener\">Terms of Use</a> and <a href=\"http://ancorathemes.com/about/\" target=\"_blank\" rel=\"noopener\">Privacy Policy</a>[/vc_wp_text][/trx_sc_content][/vc_column][/vc_row]',
				'meta' => array(
						'trx_addons_options' => array(
								'layout_type' => 'footer'
								),
						'_wpb_shortcodes_custom_css' => '.vc_custom_1489074210606{background-color: #413e49 !important;}.vc_custom_1491807515465{background-color: #393741 !important;}'
						)
				),
		'header_662' => array(
				'name' => 'Header Centered',
				'template' => '[vc_row equal_height=\"yes\" content_placement=\"middle\" row_type=\"compact\" row_delimiter=\"\" row_fixed=\"\" hide_on_tablet=\"\" hide_on_mobile=\"\" hide_on_frontpage=\"\" css=\".vc_custom_1492756907240{padding-top: 0.35em !important;padding-bottom: 0.25em !important;}\"][vc_column icons_position=\"left\"][trx_sc_content size=\"1_1\" number_position=\"br\" title_style=\"default\"][vc_row_inner equal_height=\"yes\" content_placement=\"middle\" row_type=\"compact\" row_delimiter=\"\"][vc_column_inner width=\"1/3\" column_align=\"left\" icons_position=\"left\" offset=\"vc_hidden-xs\"][trx_sc_layouts_search style=\"fullscreen\" ajax=\"\"][trx_sc_layouts_login text_login=\"Sign in\"][/vc_column_inner][vc_column_inner width=\"1/3\" column_align=\"center\" icons_position=\"left\"][trx_sc_layouts_logo logo=\"665\"][/vc_column_inner][vc_column_inner width=\"1/3\" column_align=\"right\" icons_position=\"left\" offset=\"vc_hidden-xs\"][trx_sc_layouts_cart][/vc_column_inner][/vc_row_inner][/trx_sc_content][/vc_column][/vc_row][vc_row][vc_column icons_position=\"left\"][trx_sc_content size=\"1_1\" number_position=\"br\" title_style=\"default\"][vc_separator css=\".vc_custom_1492756376422{margin-top: 0px !important;margin-bottom: 0px !important;}\"][/trx_sc_content][/vc_column][/vc_row][vc_row equal_height=\"yes\" content_placement=\"middle\" row_type=\"compact\" row_delimiter=\"\" row_fixed=\"1\" hide_on_tablet=\"\" hide_on_mobile=\"\" hide_on_frontpage=\"\" css=\".vc_custom_1492756966839{padding-top: 0.52em !important;padding-bottom: 0.35em !important;}\"][vc_column column_align=\"center\" icons_position=\"left\"][trx_sc_content size=\"1_1\" number_position=\"br\" title_style=\"default\"][trx_sc_layouts_menu location=\"none\" menu=\"main-menu\" animation_in=\"fadeInUpSmall\" animation_out=\"fadeOutDownSmall\" mobile_button=\"1\" mobile_menu=\"1\" hide_on_mobile=\"1\" burger=\"\" mobile=\"1\" stretch=\"\" mobile_hide=\"1\"][/trx_sc_content][/vc_column][/vc_row]',
				'meta' => array(
						'trx_addons_options' => array(
								'layout_type' => 'header'
								),
						'_wpb_shortcodes_custom_css' => '.vc_custom_1492756907240{padding-top: 0.35em !important;padding-bottom: 0.25em !important;}.vc_custom_1492756966839{padding-top: 0.52em !important;padding-bottom: 0.35em !important;}.vc_custom_1492756376422{margin-top: 0px !important;margin-bottom: 0px !important;}'
						)
				),
		'header_20' => array(
				'name' => 'Header Fullwidth',
				'template' => '[vc_row][vc_column icons_position=\"left\"][trx_sc_layouts layout=\"679\"][/vc_column][/vc_row][vc_row scheme=\"dark\" row_type=\"normal\" row_delimiter=\"\" row_fixed=\"\" hide_on_tablet=\"\" hide_on_mobile=\"\" hide_on_frontpage=\"1\" css=\".vc_custom_1491808544781{background-image: url(http://your-dress.ancorathemes.com/wp-content/uploads/2017/02/top.jpg?id=433) !important;background-position: center !important;background-repeat: no-repeat !important;background-size: cover !important;}\"][vc_column column_align=\"center\" icons_position=\"left\"][trx_sc_content size=\"1_1\" number_position=\"br\" title_style=\"default\" padding=\"none\"][trx_sc_layouts_title title=\"1\" meta=\"\" breadcrumbs=\"1\" icon_type=\"fontawesome\" icon_fontawesome=\"\"][/trx_sc_content][/vc_column][/vc_row]',
				'meta' => array(
						'trx_addons_options' => array(
								'layout_type' => 'header'
								),
						'_wpb_shortcodes_custom_css' => '.vc_custom_1491808544781{background-image: url(http://your-dress.ancorathemes.com/wp-content/uploads/2017/02/top.jpg?id=433) !important;background-position: center !important;background-repeat: no-repeat !important;background-size: cover !important;}'
						)
				),
		'header_679' => array(
				'name' => 'Header Fullwidth Simple',
				'template' => '[vc_row equal_height=\"yes\" content_placement=\"middle\" row_type=\"compact\" row_delimiter=\"\" row_fixed=\"1\" hide_on_tablet=\"\" hide_on_mobile=\"\" hide_on_frontpage=\"\" css=\".vc_custom_1491815391370{padding-top: 1.52em !important;padding-bottom: 1.52em !important;}\"][vc_column width=\"1/6\" column_align=\"left\" icons_position=\"left\" column_type=\"center\" css=\".vc_custom_1491815598366{margin-top: 8px !important;margin-bottom: -5px !important;}\"][trx_sc_layouts_logo][/vc_column][vc_column width=\"5/6\" column_align=\"center\" icons_position=\"left\" offset=\"vc_col-lg-8\"][trx_sc_layouts_menu location=\"none\" menu=\"main-menu\" animation_in=\"fadeInUpSmall\" animation_out=\"fadeOutDownSmall\" mobile_button=\"1\" mobile_menu=\"1\" hide_on_mobile=\"1\" burger=\"\" mobile=\"1\" stretch=\"\" mobile_hide=\"1\"][/vc_column][vc_column width=\"1/6\" column_align=\"right\" icons_position=\"left\" offset=\"vc_hidden-md vc_hidden-sm vc_hidden-xs\"][trx_sc_layouts_login hide_on_tablet=\"1\" hide_on_mobile=\"1\" text_login=\"Sign in\"][trx_sc_layouts_cart hide_on_tablet=\"1\" hide_on_mobile=\"1\"][trx_sc_layouts_search style=\"fullscreen\" ajax=\"\" hide_on_tablet=\"1\" hide_on_mobile=\"1\"][/vc_column][/vc_row]',
				'meta' => array(
						'trx_addons_options' => array(
								'layout_type' => 'header'
								),
						'_wpb_shortcodes_custom_css' => '.vc_custom_1491815391370{padding-top: 1.52em !important;padding-bottom: 1.52em !important;}.vc_custom_1491815598366{margin-top: 8px !important;margin-bottom: -5px !important;}'
						)
				),
		'custom_636' => array(
				'name' => 'How to rent',
				'template' => '[vc_row full_width=\"stretch_row_content_no_spaces\" scheme=\"dark\" css=\".vc_custom_1492591000406{background-image: url(http://your-dress.ancorathemes.com/wp-content/uploads/2017/03/bg2-1.jpg?id=326) !important;background-position: center !important;background-repeat: no-repeat !important;background-size: cover !important;}\"][vc_column width=\"1/2\" scheme=\"dark\" icons_position=\"left\" offset=\"vc_col-lg-4 vc_col-md-4 vc_col-xs-offset-1 vc_col-xs-10\"][vc_empty_space alter_height=\"huge\" hide_on_mobile=\"\" css=\".vc_custom_1492591177506{margin-top: 1.35rem !important;}\"][trx_sc_title title_style=\"default\" title_tag=\"h1\" title=\"Frequently Asked  {{Questions }} \" subtitle=\"how to rent\"][vc_empty_space height=\"1.5rem\" alter_height=\"none\" hide_on_mobile=\"\"][vc_tta_accordion shape=\"round\" color=\"white\" c_icon=\"chevron\" c_position=\"right\" active_section=\"1\" no_fill=\"true\"][vc_tta_section title=\"How does YourDress work?\" tab_id=\"1489336161918-b50963cf-bc47\"][vc_column_text]Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim.[/vc_column_text][/vc_tta_section][vc_tta_section title=\"How to rent a dress?\" tab_id=\"1489336161927-1fc2c120-859e\"][vc_column_text]Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim.[/vc_column_text][/vc_tta_section][vc_tta_section title=\"How are the dresses cleaned?\" tab_id=\"1489336240659-6ceecc4d-9920\"][vc_column_text]Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim.[/vc_column_text][/vc_tta_section][/vc_tta_accordion][vc_empty_space alter_height=\"large\" hide_on_mobile=\"\" css=\".vc_custom_1492592510805{margin-top: -0.8rem !important;}\"][/vc_column][/vc_row]',
				'meta' => array(
						'trx_addons_options' => array(
								'layout_type' => 'custom'
								),
						'_wpb_shortcodes_custom_css' => '.vc_custom_1492591000406{background-image: url(http://your-dress.ancorathemes.com/wp-content/uploads/2017/03/bg2-1.jpg?id=326) !important;background-position: center !important;background-repeat: no-repeat !important;background-size: cover !important;}.vc_custom_1492591177506{margin-top: 1.35rem !important;}.vc_custom_1492592510805{margin-top: -0.8rem !important;}'
						)
				),
		'custom_531' => array(
				'name' => 'Rent a dress',
				'template' => '<p>[vc_row][vc_column icons_position=\"left\"][product_categories orderby=\"menu_order\" order=\"ASC\" columns=\"2\" ids=\"\"][vc_empty_space height=\"1.8rem\" alter_height=\"none\" hide_on_mobile=\"\"][/vc_column][/vc_row]</p>
',
				'meta' => array(
						'trx_addons_options' => array(
								'layout_type' => 'custom'
								)
						)
				),
		'custom_633' => array(
				'name' => 'Shop',
				'template' => '[vc_row][vc_column icons_position=\"left\"][vc_empty_space alter_height=\"large\" hide_on_mobile=\"\" css=\".vc_custom_1492593062476{margin-top: 1.5rem !important;}\"][vc_row_inner][vc_column_inner width=\"2/3\" icons_position=\"left\" offset=\"vc_col-lg-6 vc_col-md-6\"][trx_sc_title title_style=\"default\" title=\"Find Gorgeous Dress {{For Any Occasion}} \" subtitle=\"for any occasion\"][/vc_column_inner][vc_column_inner width=\"1/3\" icons_position=\"left\" offset=\"vc_col-lg-6 vc_col-md-6\"][vc_empty_space alter_height=\"small\" hide_on_mobile=\"\" css=\".vc_custom_1492593147621{margin-top: 1.3rem !important;}\"][trx_sc_button type=\"default\" align=\"right\" icon_position=\"left\" icon_type=\"fontawesome\" link=\"/shop/\" title=\"view all dresses\"][vc_empty_space alter_height=\"large\" hide_on_mobile=\"\" css=\".vc_custom_1492593177861{margin-bottom: -0.3rem !important;}\"][/vc_column_inner][/vc_row_inner][products columns=\"4\" orderby=\"date\" order=\"DESC\" ids=\"275, 269, 263, 257, 251, 244, 232, 215\"][/vc_column][/vc_row]',
				'meta' => array(
						'trx_addons_options' => array(
								'layout_type' => 'custom'
								),
						'_wpb_shortcodes_custom_css' => '.vc_custom_1492593062476{margin-top: 1.5rem !important;}.vc_custom_1492593147621{margin-top: 1.3rem !important;}.vc_custom_1492593177861{margin-bottom: -0.3rem !important;}'
						)
				)
		);
?>