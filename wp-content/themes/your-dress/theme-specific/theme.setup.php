<?php
/**
 * Setup theme-specific fonts and colors
 *
 * @package WordPress
 * @subpackage YOUR_DRESS
 * @since YOUR_DRESS 1.0.22
 */

// Theme init priorities:
// 1 - register filters to add/remove lists items in the Theme Options
// 2 - create Theme Options
// 3 - add/remove Theme Options elements
// 5 - load Theme Options
// 9 - register other filters (for installer, etc.)
//10 - standard Theme init procedures (not ordered)
if ( !function_exists('your_dress_customizer_theme_setup1') ) {
	add_action( 'after_setup_theme', 'your_dress_customizer_theme_setup1', 1 );
	function your_dress_customizer_theme_setup1() {
		
		// -----------------------------------------------------------------
		// -- Theme fonts (Google and/or custom fonts)
		// -----------------------------------------------------------------
		
		// Fonts to load when theme start
		// It can be Google fonts or uploaded fonts, placed in the folder /css/font-face/font-name inside the theme folder
		// Attention! Font's folder must have name equal to the font's name, with spaces replaced on the dash '-'
		// For example: font name 'TeX Gyre Termes', folder 'TeX-Gyre-Termes'
		your_dress_storage_set('load_fonts', array(
			// Google font
			array(
				'name'	 => 'Libre Baskerville',
				'family' => 'serif',
				'styles' => '400,400i,700'		// Parameter 'style' used only for the Google fonts
				),
			// Font-face packed with theme
			array(
				'name'   => 'Raleway',
				'family' => 'serif',
				'styles' => '400,500,700'		// Parameter 'style' used only for the Google fonts
				)
		));
		
		// Characters subset for the Google fonts. Available values are: latin,latin-ext,cyrillic,cyrillic-ext,greek,greek-ext,vietnamese
		your_dress_storage_set('load_fonts_subset', 'latin,latin-ext');
		
		// Settings of the main tags
		your_dress_storage_set('theme_fonts', array(
			'p' => array(
				'title'				=> esc_html__('Main text', 'your-dress'),
				'description'		=> esc_html__('Font settings of the main text of the site', 'your-dress'),
				'font-family'		=> 'Raleway,serif',
				'font-size' 		=> '15px',
				'font-weight'		=> '500',
				'font-style'		=> 'normal',
				'line-height'		=> '1.5em',
				'text-decoration'	=> 'none',
				'text-transform'	=> 'none',
				'letter-spacing'	=> '',
				'margin-top'		=> '0em',
				'margin-bottom'		=> '1.4em'
				),
			'h1' => array(
				'title'				=> esc_html__('Heading 1', 'your-dress'),
				'font-family'		=> 'Libre Baskerville, serif',
				'font-size' 		=> '4rem',
				'font-weight'		=> '400',
				'font-style'		=> 'normal',
				'line-height'		=> '1.2em',
				'text-decoration'	=> 'none',
				'text-transform'	=> 'none',
				'letter-spacing'	=> '-2.4px',
				'margin-top'		=> '1.77em',
				'margin-bottom'		=> '1.12em'
				),
			'h2' => array(
				'title'				=> esc_html__('Heading 2', 'your-dress'),
				'font-family'		=> 'Libre Baskerville, serif',
				'font-size' 		=> '3.2rem',
				'font-weight'		=> '400',
				'font-style'		=> 'normal',
				'line-height'		=> '1em',
				'text-decoration'	=> 'none',
				'text-transform'	=> 'none',
				'letter-spacing'	=> '-2px',
				'margin-top'		=> '2.3em',
				'margin-bottom'		=> '1.1em'
				),
			'h3' => array(
				'title'				=> esc_html__('Heading 3', 'your-dress'),
				'font-family'		=> 'Libre Baskerville, serif',
				'font-size' 		=> '2.4em',
				'font-weight'		=> '400',
				'font-style'		=> 'normal',
				'line-height'		=> '1.23em',
				'text-decoration'	=> 'none',
				'text-transform'	=> 'none',
				'letter-spacing'	=> '-1.8px',
				'margin-top'		=> '3.04em',
				'margin-bottom'		=> '1em'
				),
			'h4' => array(
				'title'				=> esc_html__('Heading 4', 'your-dress'),
				'font-family'		=> 'Libre Baskerville, serif',
				'font-size' 		=> '2em',
				'font-weight'		=> '400',
				'font-style'		=> 'normal',
				'line-height'		=> '1.3em',
				'text-decoration'	=> 'none',
				'text-transform'	=> 'none',
				'letter-spacing'	=> '-1.5px',
				'margin-top'		=> '3.5565em',
				'margin-bottom'		=> '0.84em'
				),
			'h5' => array(
				'title'				=> esc_html__('Heading 5', 'your-dress'),
				'font-family'		=> 'Libre Baskerville, serif',
				'font-size' 		=> '1.6em',
				'font-weight'		=> '700',
				'font-style'		=> 'normal',
				'line-height'		=> '1.5em',
				'text-decoration'	=> 'none',
				'text-transform'	=> 'none',
				'letter-spacing'	=> '-1px',
				'margin-top'		=> '4.5em',
				'margin-bottom'		=> '0.9em'
				),
			'h6' => array(
				'title'				=> esc_html__('Heading 6', 'your-dress'),
				'font-family'		=> 'Libre Baskerville, serif',
				'font-size' 		=> '1.333em',
				'font-weight'		=> '700',
				'font-style'		=> 'normal',
				'line-height'		=> '1.4706em',
				'text-decoration'	=> 'none',
				'text-transform'	=> 'none',
				'letter-spacing'	=> '-0.8px',
				'margin-top'		=> '5.4176em',
				'margin-bottom'		=> '0.7412em'
				),
			'logo' => array(
				'title'				=> esc_html__('Logo text', 'your-dress'),
				'description'		=> esc_html__('Font settings of the text case of the logo', 'your-dress'),
				'font-family'		=> 'Libre Baskerville, serif',
				'font-size' 		=> '2.8em',
				'font-weight'		=> '400',
				'font-style'		=> 'normal',
				'line-height'		=> '1.25em',
				'text-decoration'	=> 'none',
				'text-transform'	=> 'lowercase',
				'letter-spacing'	=> '1px'
				),
			'button' => array(
				'title'				=> esc_html__('Buttons', 'your-dress'),
				'font-family'		=> 'Raleway,serif',
				'font-size' 		=> '13px',
				'font-weight'		=> '700',
				'font-style'		=> 'normal',
				'line-height'		=> '1.5em',
				'text-decoration'	=> 'none',
				'text-transform'	=> 'uppercase',
				'letter-spacing'	=> '1px'
				),
			'input' => array(
				'title'				=> esc_html__('Input fields', 'your-dress'),
				'description'		=> esc_html__('Font settings of the input fields, dropdowns and textareas', 'your-dress'),
				'font-family'		=> 'Raleway,serif',
				'font-size' 		=> '1rem',
				'font-weight'		=> '500',
				'font-style'		=> 'normal',
				'line-height'		=> '1.2em',
				'text-decoration'	=> 'none',
				'text-transform'	=> 'none',
				'letter-spacing'	=> '0.2px'
				),
			'info' => array(
				'title'				=> esc_html__('Post meta', 'your-dress'),
				'description'		=> esc_html__('Font settings of the post meta: date, counters, share, etc.', 'your-dress'),
				'font-family'		=> 'Libre Baskerville, serif',
				'font-size' 		=> '14px',
				'font-weight'		=> '400',
				'font-style'		=> 'normal',
				'line-height'		=> '1.5em',
				'text-decoration'	=> 'none',
				'text-transform'	=> 'none',
				'letter-spacing'	=> '-0.4px',
				'margin-top'		=> '0.4em',
				'margin-bottom'		=> ''
				),
			'menu' => array(
				'title'				=> esc_html__('Main menu', 'your-dress'),
				'description'		=> esc_html__('Font settings of the main menu items', 'your-dress'),
				'font-family'		=> 'Raleway, serif',
				'font-size' 		=> '13px',
				'font-weight'		=> '700',
				'font-style'		=> 'normal',
				'line-height'		=> '1.5em',
				'text-decoration'	=> 'none',
				'text-transform'	=> 'uppercase',
				'letter-spacing'	=> '1px'
				),
			'submenu' => array(
				'title'				=> esc_html__('Dropdown menu', 'your-dress'),
				'description'		=> esc_html__('Font settings of the dropdown menu items', 'your-dress'),
				'font-family'		=> 'Raleway, serif',
				'font-size' 		=> '13px',
				'font-weight'		=> '700',
				'font-style'		=> 'normal',
				'line-height'		=> '1.5em',
				'text-decoration'	=> 'none',
				'text-transform'	=> 'uppercase',
				'letter-spacing'	=> '1px'
				)
		));
		
		
		// -----------------------------------------------------------------
		// -- Theme colors for customizer
		// -- Attention! Inner scheme must be last in the array below
		// -----------------------------------------------------------------
		your_dress_storage_set('schemes', array(
		
			// Color scheme: 'default'
			'default' => array(
				'title'	 => esc_html__('Default', 'your-dress'),
				'colors' => array(
					
					// Whole block border and background
					'bg_color'				=> '#ffffff',       //
					'bd_color'				=> '#e6e6e6',       //
		
					// Text and links colors
					'text'					=> '#595c65',       //
					'text_light'			=> '#b7b7b7',       //
					'text_dark'				=> '#323a4d',       //
					'text_link'				=> '#dec0b2',       //
					'text_hover'			=> '#d5b0a0',       //
		
					// Alternative blocks (submenu, buttons, tabs, etc.)
					'alter_bg_color'		=> '#c7a89b',       //
					'alter_bg_hover'		=> '#bf9d8e',       //
					'alter_bd_color'		=> '#e5e5e5',
					'alter_bd_hover'		=> '#dadada',
					'alter_text'			=> '#d9b9ab',       //
					'alter_light'			=> '#5a5f6c',       //
					'alter_dark'			=> '#4f525a',       //
					'alter_link'			=> '#b92530',       //
					'alter_hover'			=> '#c4aea4',       //
		
					// Input fields (form's fields and textarea)
					'input_bg_color'		=> '#f6f4f0',	    //
					'input_bg_hover'		=> '#e7eaed',	    //'rgba(221,225,229,0.3)',
					'input_bd_color'		=> '#c6a79a',       //
					'input_bd_hover'		=> '#d2d0ce',       //
					'input_text'			=> '#8e8e8e',   //
					'input_light'			=> '#818283',   //
					'input_dark'			=> '#1d1d1d',
					
					// Inverse blocks (text and links on accented bg)
					'inverse_text'			=> '#ffffff',      //
					'inverse_light'			=> '#000000',
					'inverse_dark'			=> '#000000',
					'inverse_link'			=> '#94caa0',   //
					'inverse_hover'			=> '#7aa8bd',   //
		
					// Additional accented colors (if used in the current theme)
					// For example:
					//'accent2'				=> '#faef81'
				
				)
			),
		
			// Color scheme: 'dark'
			'dark' => array(
				'title'  => esc_html__('Dark', 'your-dress'),
				'colors' => array(
					
					// Whole block border and background
					'bg_color'				=> '#0e0d12',
					'bd_color'				=> '#1c1b1f',
		
					// Text and links colors
					'text'					=> '#b7b7b7',
					'text_light'			=> '#e0e0e0',   //
					'text_dark'				=> '#ffffff',   //
					'text_link'				=> '#dec0b2',   //
					'text_hover'			=> '#d5b0a0',   //
		
					// Alternative blocks (submenu, buttons, tabs, etc.)
					'alter_bg_color'		=> '#413e49',   //
					'alter_bg_hover'		=> '#f6f4f0',  //
					'alter_bd_color'		=> '#313131',
					'alter_bd_hover'		=> '#3d3d3d',
					'alter_text'			=> '#8a898a',   //
					'alter_light'			=> '#5f5f5f',
					'alter_dark'			=> '#484848',   //
					'alter_link'			=> '#ffaa5f',
					'alter_hover'			=> '#fe7259',
		
					// Input fields (form's fields and textarea)
					'input_bg_color'		=> '#2e2d32',	//'rgba(62,61,66,0.5)',
					'input_bg_hover'		=> '#2e2d32',	//'rgba(62,61,66,0.5)',
					'input_bd_color'		=> '#2e2d32',	//'rgba(62,61,66,0.5)',
					'input_bd_hover'		=> '#353535',
					'input_text'			=> '#b7b7b7',
					'input_light'			=> '#5f5f5f',
					'input_dark'			=> '#ffffff',
					
					// Inverse blocks (text and links on accented bg)
					'inverse_text'			=> '#ffffff',       //
					'inverse_light'			=> '#5f5f5f',
					'inverse_dark'			=> '#d5b0a0',       //
					'inverse_link'			=> '#ffffff',
					'inverse_hover'			=> '#1d1d1d',
				
					// Additional accented colors (if used in the current theme)
					// For example:
					//'accent2'				=> '#ff6469'
		
				)
			)
		
		));
	}
}

			
// Additional (calculated) theme-specific colors
// Attention! Don't forget setup custom colors also in the theme.customizer.color-scheme.js
if (!function_exists('your_dress_customizer_add_theme_colors')) {
	function your_dress_customizer_add_theme_colors($colors) {
		if (substr($colors['text'], 0, 1) == '#') {
			$colors['bg_color_0']  = your_dress_hex2rgba( $colors['bg_color'], 0 );
			$colors['bg_color_02']  = your_dress_hex2rgba( $colors['bg_color'], 0.2 );
            $colors['inverse_text_02']  = your_dress_hex2rgba( $colors['inverse_text'], 0.2 );
			$colors['bg_color_07']  = your_dress_hex2rgba( $colors['bg_color'], 0.7 );
			$colors['bg_color_08']  = your_dress_hex2rgba( $colors['bg_color'], 0.8 );
			$colors['bg_color_09']  = your_dress_hex2rgba( $colors['bg_color'], 0.9 );
			$colors['alter_bg_color_07']  = your_dress_hex2rgba( $colors['alter_bg_color'], 0.7 );
			$colors['alter_bg_color_04']  = your_dress_hex2rgba( $colors['alter_bg_color'], 0.4 );
			$colors['alter_bg_color_02']  = your_dress_hex2rgba( $colors['alter_bg_color'], 0.2 );
			$colors['alter_bd_color_02']  = your_dress_hex2rgba( $colors['alter_bd_color'], 0.2 );
            $colors['input_bd_hover_04']  = your_dress_hex2rgba( $colors['input_bd_hover'], 0.4 );
            $colors['alter_bg_hover_01']  = your_dress_hex2rgba( $colors['alter_bg_hover'], 0.1 );
            $colors['input_bg_color_01']  = your_dress_hex2rgba( $colors['input_bg_color'], 0.1 );
			$colors['text_dark_07']  = your_dress_hex2rgba( $colors['text_dark'], 0.7 );
            $colors['text_dark_05']  = your_dress_hex2rgba( $colors['text_dark'], 0.5 );
            $colors['text_dark_02']  = your_dress_hex2rgba( $colors['text_dark'], 0.2 );
			$colors['text_link_02']  = your_dress_hex2rgba( $colors['text_link'], 0.2 );
			$colors['text_link_07']  = your_dress_hex2rgba( $colors['text_link'], 0.7 );
			$colors['text_link_blend'] = your_dress_hsb2hex(your_dress_hex2hsb( $colors['text_link'], 2, -5, 5 ));
			$colors['alter_link_blend'] = your_dress_hsb2hex(your_dress_hex2hsb( $colors['alter_link'], 2, -5, 5 ));
		} else {
			$colors['bg_color_0'] = '{{ data.bg_color_0 }}';
			$colors['bg_color_02'] = '{{ data.bg_color_02 }}';
			$colors['bg_color_07'] = '{{ data.bg_color_07 }}';
			$colors['bg_color_08'] = '{{ data.bg_color_08 }}';
			$colors['bg_color_09'] = '{{ data.bg_color_09 }}';
			$colors['alter_bg_color_07'] = '{{ data.alter_bg_color_07 }}';
			$colors['alter_bg_color_04'] = '{{ data.alter_bg_color_04 }}';
			$colors['alter_bg_color_02'] = '{{ data.alter_bg_color_02 }}';
			$colors['alter_bd_color_02'] = '{{ data.alter_bd_color_02 }}';
			$colors['text_dark_07'] = '{{ data.text_dark_07 }}';
			$colors['text_link_02'] = '{{ data.text_link_02 }}';
			$colors['text_link_07'] = '{{ data.text_link_07 }}';
			$colors['text_link_blend'] = '{{ data.text_link_blend }}';
			$colors['alter_link_blend'] = '{{ data.alter_link_blend }}';
		}
		return $colors;
	}
}


			
// Additional theme-specific fonts rules
// Attention! Don't forget setup fonts rules also in the theme.customizer.color-scheme.js
if (!function_exists('your_dress_customizer_add_theme_fonts')) {
	function your_dress_customizer_add_theme_fonts($fonts) {
		$rez = array();	
		foreach ($fonts as $tag => $font) {
			//$rez[$tag] = $font;
			if (substr($font['font-family'], 0, 2) != '{{') {
				$rez[$tag.'_font-family'] 		= !empty($font['font-family']) && !your_dress_is_inherit($font['font-family'])
														? 'font-family:' . trim($font['font-family']) . ';' 
														: '';
				$rez[$tag.'_font-size'] 		= !empty($font['font-size']) && !your_dress_is_inherit($font['font-size'])
														? 'font-size:' . your_dress_prepare_css_value($font['font-size']) . ";"
														: '';
				$rez[$tag.'_line-height'] 		= !empty($font['line-height']) && !your_dress_is_inherit($font['line-height'])
														? 'line-height:' . trim($font['line-height']) . ";"
														: '';
				$rez[$tag.'_font-weight'] 		= !empty($font['font-weight']) && !your_dress_is_inherit($font['font-weight'])
														? 'font-weight:' . trim($font['font-weight']) . ";"
														: '';
				$rez[$tag.'_font-style'] 		= !empty($font['font-style']) && !your_dress_is_inherit($font['font-style'])
														? 'font-style:' . trim($font['font-style']) . ";"
														: '';
				$rez[$tag.'_text-decoration'] 	= !empty($font['text-decoration']) && !your_dress_is_inherit($font['text-decoration'])
														? 'text-decoration:' . trim($font['text-decoration']) . ";"
														: '';
				$rez[$tag.'_text-transform'] 	= !empty($font['text-transform']) && !your_dress_is_inherit($font['text-transform'])
														? 'text-transform:' . trim($font['text-transform']) . ";"
														: '';
				$rez[$tag.'_letter-spacing'] 	= !empty($font['letter-spacing']) && !your_dress_is_inherit($font['letter-spacing'])
														? 'letter-spacing:' . trim($font['letter-spacing']) . ";"
														: '';
				$rez[$tag.'_margin-top'] 		= !empty($font['margin-top']) && !your_dress_is_inherit($font['margin-top'])
														? 'margin-top:' . your_dress_prepare_css_value($font['margin-top']) . ";"
														: '';
				$rez[$tag.'_margin-bottom'] 	= !empty($font['margin-bottom']) && !your_dress_is_inherit($font['margin-bottom'])
														? 'margin-bottom:' . your_dress_prepare_css_value($font['margin-bottom']) . ";"
														: '';
			} else {
				$rez[$tag.'_font-family']		= '{{ data["'.$tag.'_font-family"] }}';
				$rez[$tag.'_font-size']			= '{{ data["'.$tag.'_font-size"] }}';
				$rez[$tag.'_line-height']		= '{{ data["'.$tag.'_line-height"] }}';
				$rez[$tag.'_font-weight']		= '{{ data["'.$tag.'_font-weight"] }}';
				$rez[$tag.'_font-style']		= '{{ data["'.$tag.'_font-style"] }}';
				$rez[$tag.'_text-decoration']	= '{{ data["'.$tag.'_text-decoration"] }}';
				$rez[$tag.'_text-transform']	= '{{ data["'.$tag.'_text-transform"] }}';
				$rez[$tag.'_letter-spacing']	= '{{ data["'.$tag.'_letter-spacing"] }}';
				$rez[$tag.'_margin-top']		= '{{ data["'.$tag.'_margin-top"] }}';
				$rez[$tag.'_margin-bottom']		= '{{ data["'.$tag.'_margin-bottom"] }}';
			}
		}
		return $rez;
	}
}


//-------------------------------------------------------
//-- Thumb sizes
//-------------------------------------------------------

if ( !function_exists('your_dress_customizer_theme_setup') ) {
	add_action( 'after_setup_theme', 'your_dress_customizer_theme_setup' );
	function your_dress_customizer_theme_setup() {

		// Enable support for Post Thumbnails
		add_theme_support( 'post-thumbnails' );
		set_post_thumbnail_size(370, 0, false);
		
		// Add thumb sizes
		// ATTENTION! If you change list below - check filter's names in the 'trx_addons_filter_get_thumb_size' hook
		$thumb_sizes = apply_filters('your_dress_filter_add_thumb_sizes', array(
			'your_dress-thumb-huge'		=> array(1170, 658, true),
			'your_dress-thumb-big' 		=> array( 1155, 615, true),
			'your_dress-thumb-med' 		=> array( 555, 360, true),
            'your_dress-thumb-serv' 		=> array( 732, 660, true),
            'your_dress-thumb-team' 		=> array( 740, 892, true),
                'your_dress-thumb-blog' 		=> array( 740, 540, true),
			'your_dress-thumb-tiny' 		=> array(  90,  90, true),
			'your_dress-thumb-masonry-big' => array( 760,   0, false),		// Only downscale, not crop
			'your_dress-thumb-masonry'		=> array( 370,   0, false),		// Only downscale, not crop
			)
		);
		$mult = your_dress_get_theme_option('retina_ready', 1);
		if ($mult > 1) $GLOBALS['content_width'] = apply_filters( 'your_dress_filter_content_width', 1170*$mult);
		foreach ($thumb_sizes as $k=>$v) {
			// Add Original dimensions
			add_image_size( $k, $v[0], $v[1], $v[2]);
			// Add Retina dimensions
			if ($mult > 1) add_image_size( $k.'-@retina', $v[0]*$mult, $v[1]*$mult, $v[2]);
		}

	}
}

if ( !function_exists('your_dress_customizer_image_sizes') ) {
	add_filter( 'image_size_names_choose', 'your_dress_customizer_image_sizes' );
	function your_dress_customizer_image_sizes( $sizes ) {
		$thumb_sizes = apply_filters('your_dress_filter_add_thumb_sizes', array(
			'your_dress-thumb-huge'		=> esc_html__( 'Fullsize image', 'your-dress' ),
			'your_dress-thumb-big'			=> esc_html__( 'Large image', 'your-dress' ),
			'your_dress-thumb-med'			=> esc_html__( 'Medium image', 'your-dress' ),
			'your_dress-thumb-tiny'		=> esc_html__( 'Small square avatar', 'your-dress' ),
			'your_dress-thumb-masonry-big'	=> esc_html__( 'Masonry Large (scaled)', 'your-dress' ),
			'your_dress-thumb-masonry'		=> esc_html__( 'Masonry (scaled)', 'your-dress' ),
			)
		);
		$mult = your_dress_get_theme_option('retina_ready', 1);
		foreach($thumb_sizes as $k=>$v) {
			$sizes[$k] = $v;
			if ($mult > 1) $sizes[$k.'-@retina'] = $v.' '.esc_html__('@2x', 'your-dress' );
		}
		return $sizes;
	}
}

// Remove some thumb-sizes from the ThemeREX Addons list
if ( !function_exists( 'your_dress_customizer_trx_addons_add_thumb_sizes' ) ) {
	add_filter( 'trx_addons_filter_add_thumb_sizes', 'your_dress_customizer_trx_addons_add_thumb_sizes');
	function your_dress_customizer_trx_addons_add_thumb_sizes($list=array()) {
		if (is_array($list)) {
			foreach ($list as $k=>$v) {
				if (in_array($k, array(
								'trx_addons-thumb-huge',
								'trx_addons-thumb-big',
								'trx_addons-thumb-medium',
								'trx_addons-thumb-tiny',
								'trx_addons-thumb-masonry-big',
								'trx_addons-thumb-masonry',
								)
							)
						) unset($list[$k]);
			}
		}
		return $list;
	}
}

// and replace removed styles with theme-specific thumb size
if ( !function_exists( 'your_dress_customizer_trx_addons_get_thumb_size' ) ) {
	add_filter( 'trx_addons_filter_get_thumb_size', 'your_dress_customizer_trx_addons_get_thumb_size');
	function your_dress_customizer_trx_addons_get_thumb_size($thumb_size='') {
		return str_replace(array(
							'trx_addons-thumb-huge',
							'trx_addons-thumb-huge-@retina',
							'trx_addons-thumb-big',
							'trx_addons-thumb-big-@retina',
							'trx_addons-thumb-medium',
							'trx_addons-thumb-medium-@retina',
                            'trx_addons-thumb-serv',
                            'trx_addons-thumb-serv-@retina',
                'trx_addons-thumb-team',
                'trx_addons-thumb-team-@retina',
                'trx_addons-thumb-blog',
                'trx_addons-thumb-blog-@retina',
							'trx_addons-thumb-tiny',
							'trx_addons-thumb-tiny-@retina',
							'trx_addons-thumb-masonry-big',
							'trx_addons-thumb-masonry-big-@retina',
							'trx_addons-thumb-masonry',
							'trx_addons-thumb-masonry-@retina',
							),
							array(
							'your_dress-thumb-huge',
							'your_dress-thumb-huge-@retina',
							'your_dress-thumb-big',
							'your_dress-thumb-big-@retina',
							'your_dress-thumb-med',
							'your_dress-thumb-med-@retina',
                                'your_dress-thumb-serv',
                                'your_dress-thumb-serv-@retina',
                                'your_dress-thumb-team',
                                'your_dress-thumb-team-@retina',
                                'your_dress-thumb-blog',
                                'your_dress-thumb-blog-@retina',
							'your_dress-thumb-tiny',
							'your_dress-thumb-tiny-@retina',
							'your_dress-thumb-masonry-big',
							'your_dress-thumb-masonry-big-@retina',
							'your_dress-thumb-masonry',
							'your_dress-thumb-masonry-@retina',
							),
							$thumb_size);
	}
}
?>