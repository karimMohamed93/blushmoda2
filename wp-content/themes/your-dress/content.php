<?php
/**
 * The default template to display the content of the single post, page or attachment
 *
 * Used for index/archive/search.
 *
 * @package WordPress
 * @subpackage YOUR_DRESS
 * @since YOUR_DRESS 1.0
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class( 'post_item_single post_type_'.esc_attr(get_post_type()) 
												. ' post_format_'.esc_attr(str_replace('post-format-', '', get_post_format())) 
												. ' itemscope'
												); ?>
		itemscope itemtype="http://schema.org/<?php echo esc_attr(is_single() ? 'BlogPosting' : 'Article'); ?>">
	<?php
	// Structured data snippets
	if (your_dress_is_on(your_dress_get_theme_option('seo_snippets'))) {
		?>
		<div class="structured_data_snippets">
			<meta itemprop="headline" content="<?php echo esc_attr(get_the_title()); ?>">
			<meta itemprop="datePublished" content="<?php echo esc_attr(get_the_date('Y-m-d')); ?>">
			<meta itemprop="dateModified" content="<?php echo esc_attr(get_the_modified_date('Y-m-d')); ?>">
			<meta itemscope itemprop="mainEntityOfPage" itemType="https://schema.org/WebPage" itemid="<?php echo esc_url(get_the_permalink()); ?>" content="<?php echo esc_attr(get_the_title()); ?>"/>	
			<div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
				<div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
					<?php 
					$your_dress_logo_image = your_dress_get_retina_multiplier(2) > 1 
										? your_dress_get_theme_option( 'logo_retina' )
										: your_dress_get_theme_option( 'logo' );
					if (!empty($your_dress_logo_image)) {
						$your_dress_attr = your_dress_getimagesize($your_dress_logo_image);
						?>
						<img itemprop="url" src="<?php echo esc_url($your_dress_logo_image); ?>">
						<meta itemprop="width" content="<?php echo esc_attr($your_dress_attr[0]); ?>">
						<meta itemprop="height" content="<?php echo esc_attr($your_dress_attr[1]); ?>">
						<?php
					}
					?>
				</div>
				<meta itemprop="name" content="<?php echo esc_attr(get_bloginfo( 'name' )); ?>">
				<meta itemprop="telephone" content="">
				<meta itemprop="address" content="">
			</div>
		</div>
		<?php
	}

    // Title and post meta
    if (get_the_title() != '') {
        ?>
        <div class="post_header entry-header">
            <?php
            do_action('your_dress_action_before_post_meta');
            // Post meta
            your_dress_show_post_meta(array(
                    'categories' => true,
                    'date' => false,
                    'author' => false,
                    'edit' => false,
                    'seo' => false,
                    'share' => false,
                    'counters' => 'comments'	//comments,likes,views - comma separated in any combination
                )
            );
            do_action('your_dress_action_before_post_title');

            if(!has_post_thumbnail()) {
                $dt = apply_filters('your_dress_filter_get_post_date', your_dress_get_date('', 'j M'));
                $date = explode(' ', $dt);
                if (count($date) > 1) {
                    echo '<div class="post-date">';
                    echo '<span class="post-day">' . esc_html($date[0]) .'</span>';
                    echo '<span class="post-month">' . esc_html($date[1]) .'</span>';
                    echo '</div>';
                }
            }
            // Post title
            the_title( sprintf( '<h3 class="post_title entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h3>' );
            ?>
        </div><!-- .post_header --><?php
    }

    if(has_post_thumbnail()) {
    ?><div class="post-featured"><?php
        $dt = apply_filters('your_dress_filter_get_post_date', your_dress_get_date('', 'j M'));
        $date = explode(' ', $dt);
        if (count($date) > 1) {
            echo '<div class="post-date">';
            echo '<span class="post-day">' . esc_html($date[0]) .'</span>';
            echo '<span class="post-month">' . esc_html($date[1]) .'</span>';
            echo '</div>';
        }
        }
        // Featured image
        your_dress_show_post_featured(array( 'thumb_size' => your_dress_get_thumb_size( strpos(your_dress_get_theme_option('body_style'), 'full')!==false ? 'full' : 'big' ) ));

        if(has_post_thumbnail()) {
        ?></div><?php
}

	// Post content
	?>
	<div class="post_content entry-content" itemprop="articleBody">
		<?php
			the_content( );

			wp_link_pages( array(
				'before'      => '<div class="page_links"><span class="page_links_title">' . esc_html__( 'Pages:', 'your-dress' ) . '</span>',
				'after'       => '</div>',
				'link_before' => '<span>',
				'link_after'  => '</span>',
				'pagelink'    => '<span class="screen-reader-text">' . esc_html__( 'Page', 'your-dress' ) . ' </span>%',
				'separator'   => '<span class="screen-reader-text">, </span>',
			) );

        // Taxonomies and share
        if ( is_single() && !is_attachment() ) {
            ?>
            <div class="post_meta post_meta_single"><?php

                // Post taxonomies
                ?><div class="single-meta"><?php

                    if( get_the_tags() ) {
                        // Post taxonomies
                        ?><span class="post_meta_item post_tags"><span
                            class="post_meta_label"><?php esc_html_e('Tags:', 'your-dress'); ?></span> <?php the_tags('', ', ', ''); ?></span>
                    <?php

                    }

                    ?><span class="post_meta_item"><span
                                class="post_meta_label"><?php esc_html_e('Author:', 'your-dress'); ?></span> <?php echo esc_html__('by', 'your-dress') .' '.get_the_author();?></span>
                    <?php


                    ?> </div> <?php

                // Share
                your_dress_show_share_links(array(
                    'type' => 'block',
                    'caption' => '',
                    'before' => '<span class="post_meta_item post_share">',
                    'after' => '</span>'
                ));
                ?>
            </div>
        <?php
        }
		?>
	</div><!-- .entry-content -->

	<?php
		// Author bio.
		if ( is_single() && !is_attachment() && get_the_author_meta( 'description' ) ) {	// && is_multi_author()
			get_template_part( 'templates/author-bio' );
		}
	?>
</article>
