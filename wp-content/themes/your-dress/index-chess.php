<?php
/**
 * The template for homepage posts with "Chess" style
 *
 * @package WordPress
 * @subpackage YOUR_DRESS
 * @since YOUR_DRESS 1.0
 */

your_dress_storage_set('blog_archive', true);

get_header(); 

if (have_posts()) {

	echo get_query_var('blog_archive_start');

	$your_dress_stickies = is_home() ? get_option( 'sticky_posts' ) : false;
	$your_dress_sticky_out = is_array($your_dress_stickies) && count($your_dress_stickies) > 0 && get_query_var( 'paged' ) < 1;
	if ($your_dress_sticky_out) {
		?><div class="sticky_wrap columns_wrap"><?php	
	}
	if (!$your_dress_sticky_out) {
		?><div class="chess_wrap posts_container"><?php
	}
	while ( have_posts() ) { the_post(); 
		if ($your_dress_sticky_out && !is_sticky()) {
			$your_dress_sticky_out = false;
			?></div><div class="chess_wrap posts_container"><?php
		}
		get_template_part( 'content', $your_dress_sticky_out && is_sticky() ? 'sticky' :'chess' );
	}
	
	?></div><?php

	your_dress_show_pagination();

	echo get_query_var('blog_archive_end');

} else {

	if ( is_search() )
		get_template_part( 'content', 'none-search' );
	else
		get_template_part( 'content', 'none-archive' );

}

get_footer();
?>